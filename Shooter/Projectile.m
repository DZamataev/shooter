//
//  Projectile.m
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Projectile.h"
#import "CollisionManager.h"
#import "SHProjectileTrail.h"

#define PROJECTILE_B2D_COLLISION_ENABLED

@implementation Projectile
@synthesize type = _type, speed = _speed;
@synthesize myInfo = _myInfo;
@synthesize destroyed = _destroyed;
//@synthesize mstreak = _mstreak;
-(id)initWithType:(projectileType)incType andSpeedMod:(float)incSpeedMod withAngle:(float)angle onLayer:(CCLayer *)layer
{
    self = [super init];
    _destroyed = _prevPosDefined = NO;
    _type = incType;
    _move = ccp(0,0);
    // "100" is the speed accroding to type
    _speed = 100 * incSpeedMod;
    // define sprite here
    sprite = [CCSprite spriteWithFile:@"Player/Fireball.png"];
    [self addChild:sprite];
    sprite.opacity = 0;
    sprite.rotation = -angle;
    //
    _move = ccpForAngle(CC_DEGREES_TO_RADIANS(angle+90));
    _move = ccpMult(_move, _speed);
    _previousPosition = CGPointZero;
//    self.mstreak = nil;
    _trail = nil;
    _layer = layer;
//    NSLog(@"Streak gen: %f,%f, self pos: %f,%f",_mstreak.position.x,_mstreak.position.y,self.position.x,self.position.y);
    return self;
}

-(void)onEnter
{
    [super onEnter];
    [self scheduleUpdate];
    sprite.opacity = 255;
}

-(void)update:(ccTime)dt
{
    if (_trail == nil)
    {
//        self.mstreak = [CCMotionStreak streakWithFade:0.2 minSeg:3 width:8 color:ccc3(255, 255, 255) textureFilename:@"Icon.png"];
        _trail = [SHProjectileTrail trailNodeWithMotionStreakWithFade:0.2 minSeg:3 width:32 textureFilename:@"Player/trail.png" onLayer:_layer withPosition:_myInfo.position];
        [_trail retain];
    }
    else {
        [_trail.motionStreak setPosition:_myInfo.position];
    }
//    NSLog(@"SELF PROJECTILE RETAIN COUNT on update:%i",[self retainCount]);
    if (!_prevPosDefined)
    {
        if (!CGPointEqualToPoint(_previousPosition, CGPointZero))
            _prevPosDefined = YES;
    }
    else
    {
        CGPoint diff = ccpSub(_myInfo.position, _previousPosition);
        float ang = ccpAngleSigned(ccp(0,1), diff);
        sprite.rotation = CC_RADIANS_TO_DEGREES(-ang);
    }
    
    _previousPosition = _myInfo.position;
    
    CGPoint nextPosition = _myInfo.position;
    _myInfo.b2impulse = _move;
//    NSLog(@"proj impulse is: %f,%f", _myInfo.b2impulse.x,_myInfo.b2impulse.y);
    bool destroyCheck = NO;
    

#ifdef PROJECTILE_B2D_COLLISION_ENABLED
    if (_myInfo.b2dContactDetected)
    {
        _myInfo.bounces--;
        _myInfo.b2dContactDetected = NO;
        if (_myInfo.bounces < 0)
        {
            destroyCheck = YES;
        }
    }

#endif
    
    if (!destroyCheck)
    {
        self.position = nextPosition;
    }
    else
    {
        
        [self removeAllChildrenWithCleanup:YES];
        [self removeFromParentAndCleanup:YES];
//            NSLog(@"SELF PROJECTILE RETAIN COUNT AFTER DESTROY:%i",[self retainCount]);
        _destroyed = YES;
    }
    
}

-(void)destroy
{
//    CCLOG(@"Projectile info retain count is: %i",[_myInfo retainCount]);
    [_myInfo release]; // that will call b2body destroy;
    _myInfo = nil;
}

-(void)dealloc
{
//    [self.mstreak removeFromParentAndCleanup:YES];
//    self.mstreak = nil;
    [_trail destroyInTimeUnattached:1.0f];
    [_trail removeFromParentAndCleanup:NO];
    [self destroy];
    [super dealloc];
}
@end
