//
//  SHPlayerBehavior.h
//  Anti-heroes Wars
//
//  Created by Denis on 4/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SHPlayerMovementDelegate.h"
#import "cocos2d.h"

@class Player;
@class PlayerBody;
@class PlayerLegs;




@interface SHPlayerBehavior : NSObject <SHPlayerMovementDelegate>
{
    Player *_player;
    PlayerLegs *_legs;
    PlayerBody *_body;
    
    CCSequence *_bodySeq;
    CCSequence *_legsSeq;
    CCCallFunc *_bodyNotifyEnd;
    CCCallFunc *_legsNotifyEnd;
    CCCallFunc *_bodyCheckEnd;
    CCCallFunc *_legsCheckEnd;
    bool _gonnaStop;
}
@property (nonatomic, assign) Player *player;
@property (nonatomic, assign) PlayerLegs *legs;
@property (nonatomic, assign) PlayerBody *body;

-(id)initWithPlayer:(Player*)p andLegs:(PlayerLegs*)l andBody:(PlayerBody*)b;

-(void)standStill;
-(void)startRunLoop;
-(void)movingStatusChangedWithBool:(bool)isMoving;
-(void)notifyLegsSeqEnd;
-(void)notifyBodySeqEnd;
@end
