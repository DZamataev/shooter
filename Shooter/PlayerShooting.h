//
//  PlayerShooting.h
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlayerBody.h"
#import "CCNode.h"

@class Player;

@interface PlayerShooting : CCNode
{
    PlayerBody *_body;
    CCLayer *_layer;
    float _timeAccum;
    bool _projectileArmed;
    Player *_player;
    NSMutableArray *_firePorts;
}
-(id)initWithPlayerControlledBody:(PlayerBody *)bod andLayer:(CCLayer *)lay andPlayer:(Player*)player andFireports:(NSMutableArray*)fports;
-(void)tick:(float)dt;
@end
