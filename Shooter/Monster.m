//
//  Monster.m
//  Anti-heroes Wars
//
//  Created by Denis on 4/8/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Monster.h"


@implementation Monster
@synthesize state = _state;
@synthesize monsterClass = _monsterClass;
@synthesize actStill = _actStill;
@synthesize actWalk = _actWalk;
@synthesize actAttack = _actAttack;
@synthesize actDie = _actDie;
@synthesize feelRange = _feelRange;
@synthesize myInfo = _myInfo;
-(id)init
{
    self = [super init];
    self.state = MSStill;
    self.actWalk = self.actStill = self.actDie = self.actAttack = nil;
    self.feelRange = 1;
    self.myInfo = nil;
    return self;
}

-(void)dealloc
{
    self.actStill = nil;
    self.actWalk = nil;
    self.actAttack = nil;
    self.actDie = nil;
    self.myInfo = nil;
    [super dealloc];
}
@end
