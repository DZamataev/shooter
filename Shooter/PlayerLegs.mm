//
//  PlayerLegs.mm
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "PlayerLegs.h"
#import "CollisionManager.h"
#import "SHb2dCollisionManager.h"



@implementation PlayerLegs
@synthesize normal = _normal;
@synthesize myInfo = _myInfo;
@synthesize stillStep = _stillStep;
@synthesize leftStep = _leftStep;
@synthesize rightStep = _rightStep;
@synthesize movementDelegate = _movementDelegate;
-(id)initWithBody:(PlayerBody*)body
{
    self = [super init];
    _joyPosition = ccp(0,0);
    _previousPos = ccp(0,0);
    self.isMoving = NO;
    self.movementDelegate = nil;
    sprite = [CCSprite spriteWithFile:@"Player/Legs.png"];
    [self addChild:sprite];
    
  //  sprite.scale = 0.5f;
    myBody = body;
    _normal = ccp(0,1);
    return self;
}

-(void)onEnter
{
    [super onEnter];
    [self scheduleUpdate];
}

-(void)runStepActionOnSprite:(CCAction*)action;
{
    [sprite runAction:action];
}

-(void)stopAllActionsOnSprite
{
    [sprite stopAllActions];
}

-(bool)isMoving
{
    return _isMoving;
}

-(void)setIsMoving:(bool)incIsMoving
{
    if (_isMoving != incIsMoving && self.movementDelegate != nil)
    {
        [self.movementDelegate movingStatusChangedWithBool:incIsMoving];
    }
    _isMoving = incIsMoving;
}

-(bool)comparePoint:(CGPoint)p1 andPoint:(CGPoint)p2 withEpsilon:(CGPoint)e
{
    return (fabsf(p1.x-p2.x)<e.x && fabsf(p1.y-p2.y)<e.y);
}

-(void)update:(ccTime)dt
{
    CGPoint mip = _myInfo.position;
    CGPoint prp = _previousPos;
    CGPoint epsilon = ccp(1,1);
    if ([self comparePoint:mip andPoint:prp withEpsilon:epsilon])
        self.isMoving = NO;
    else
        self.isMoving = YES;
    _previousPos = _myInfo.position;

    
    [super setPosition:_myInfo.position];
    if (!myBody.bodyJoystickIsTouched)
    {
        [myBody setPosition:ccp(_joyPosition.x,_joyPosition.y)]; // this method wont reposition body, but will act like a joystick
    }
    [myBody repositionBody:self.position];
    
    if (_joyPosition.x!=0 || _joyPosition.y !=0)
    {
        
        //CCLOG(@"Player update: selfPos:x%f,y%f, joyPos*dt:x%f,y%f",self.position.x,self.position.y,_joyPosition.x*dt,_joyPosition.y*dt);
        CGPoint myPoint = self.position;
        CGPoint pt = ccpSub(myPoint, _joyPosition);
        CGPoint convPos = ccp(pt.x,-pt.y);
        float angle = ccpAngleSigned(_normal, ccpNormalize(convPos));
        CGFloat fi = angle*180/M_PI;
        sprite.rotation = fi;
            
        if (_speedControlFromJoy.x !=0 || _speedControlFromJoy.y != 0)
        {
            _myInfo.b2bvelocity = _speedControlFromJoy;
            _speedControlFromJoy = CGPointZero;
        }

        // dont forget to make joyPos zero
        _joyPosition = CGPointZero;
    }
    //NSLog(@"Legs think their pos is %f,%f",self.position.x,self.position.y);
}

-(void)setPosition:(CGPoint)position
{
    if (_joyPosition.x == 0 || _joyPosition.y == 0)
        _joyPosition = position;
}

-(void)receiveControlFromJoystick:(CGPoint)point
{
    _speedControlFromJoy = point;
}

-(void)repositionLegs:(CGPoint)position
{
    CGPoint pt = ccpSub(self.position, position);
    CGPoint convPos = ccp(pt.x,-pt.y);
    float angle = ccpAngleSigned(_normal, ccpNormalize(convPos));
    CGFloat fi = angle*180/M_PI;
    sprite.rotation = fi;
    
    if (_myInfo){
        _myInfo.position = position;
    }
    [super setPosition:position];
}

-(PlayerActorInfo*)setupPhysicsBody
{
    _myInfo = [[PlayerActorInfo alloc] initWithPosition:self.position andRotation:sprite.rotation andRect:sprite.boundingBox andGotType:gotDynamic andAttType:attPlayer];
    _myInfo.hostObject = self;
    [[SHb2dCollisionManager sharedSHb2dCollisionManager] addBoxBodyWithCircleShapeForGameObjectWithInfo:_myInfo];
    CCLOG(@"Player setup physics done.");
    return _myInfo;
}

-(void)dealloc
{
    self.stillStep = nil;
    self.rightStep = nil;
    self.leftStep = nil;
    [super dealloc];
}
@end
