//
//  HelloWorldLayer.mm
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// Import the interfaces
#import "GameplayLayer.h"
#import "SHb2dCollisionManager.h"
#import "ViewportController.h"
#import "SHPvPLogic.h"
#import "Projectile.h"
#import "SHBaseHealth.h"
#import "Base.h"
#import "SDrawableLayer.h"
#import "SHPlayerManager.h"
#import "SHZombieInhibitor.h"

// HelloWorldLayer implementation
@implementation GameplayLayer
@synthesize projectiles=_projectiles, isOnSlowMo;
@synthesize tileMap = _tileMap;
@synthesize background = _background;
@synthesize objects = _objects;
@synthesize meta = _meta;
@synthesize HUDLayer = _HUDLayer;
@synthesize PvPLogic = _PvPLogic;
@synthesize myHaloLayer = _myHaloLayer;

+(CCScene *) scene
{
    //CGSize winSize = [CCDirector sharedDirector].winSize;
    
	CCScene *scene = [CCScene node];
    
    // add main gameplay layer
	GameplayLayer *layer = [GameplayLayer node];
	[scene addChild: layer z:2];
	
    // add HUD
    HUDLayer *_HUDLayer = [HUDLayer node];
    _HUDLayer.gameplayLayer = layer;
    layer.HUDLayer = _HUDLayer;
    [scene addChild:_HUDLayer z:10];
    
    // add drawable
//    SDrawableLayer *dLayer = [SDrawableLayer layerWithPlume];
//    [scene addChild:dLayer z:3];
//    layer.myHaloLayer = dLayer;
    
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	if( (self=[super init])) {
        
        // zeroing values
        isOnSlowMo = NO;
        _slowMoToggle = NO;
        _slowMoTimeScaleBeginValue = 1;
        _slowMoTimeScaleTargetValue = 0.2f;
        _projectiles = [[NSMutableArray alloc] init];
        _myHaloLayer = nil;
        _testZombieInhibitor = nil;
        //
        

        
        //CGPoint tileCoord = [self tileCoordForPosition:position];
        


        
        //TILED MAP TEST
        
        _tileMap = [CCTMXTiledMap tiledMapWithTMXFile:@"Locations/Survival/Lab.tmx"];
//        self.background = [_tileMap layerNamed:@"Background"];
//        self.background.visible = YES;
//        if (self.background == nil)
//        {
//            NSLog(@"HU");
//        }
        _objects = [_tileMap layerNamed:@"Obstacles"];
        
        
        _meta = [_tileMap layerNamed:@"Meta"];
        _meta.visible = NO;
//        
//        CCTMXObjectGroup *objects = [_tileMap objectGroupNamed:@"Objects"];
//        NSAssert(objects != nil, @"'Objects' object group not found");
//        NSMutableDictionary *spawnPoint = [objects objectNamed:@"SpawnPoint"];        
//        NSAssert(spawnPoint != nil, @"SpawnPoint object not found");
//        int x = [[spawnPoint valueForKey:@"x"] intValue];
//        int y = [[spawnPoint valueForKey:@"y"] intValue];
        
//        self.player = [CCSprite spriteWithFile:@"Player.png"];
//        _player.position = ccp(x, y);
//        [self addChild:_player]; 
        
//        [self setViewpointCenter:_player.position];
        
        [self addChild:_tileMap z:-1];

        //
        
        // Tuning the collision manager
//        _collisionManager = [CollisionManager sharedCollisionManager];
//        _collisionManager.meta = _meta;
//        _collisionManager.tileMap = _tileMap;
//        [_collisionManager gatherCollidableTiles];
        //
        



        
        // set self to be layer for viewport
        [ViewportController sharedViewportController].layer = self;
        
        
        
        [self scheduleUpdate];
        
	}
	return self;
}

-(void)readObjects
{
    _tileMapObjectsInfo = [SHTileMapObjectProcessor getInfoForTileMap:_tileMap withObjectsLayerNamed:@"Entities"];
//    CCTMXObjectGroup *objects = [_tileMap objectGroupNamed:@"Objects"];
//    NSAssert(objects != nil, @"'Objects' object group not found");
//    NSMutableDictionary *firstPlayerStartPoint = [objects objectNamed:@"FirstPlayerStartPoint"];        
//    NSAssert(firstPlayerStartPoint != nil, @"FirstPlayerStartPoint object not found");
//    NSMutableDictionary *secondPlayerStartPoint = [objects objectNamed:@"SecondPlayerStartPoint"];        
//    NSAssert(secondPlayerStartPoint != nil, @"SecondPlayerStartPoint object not found");
//    NSMutableDictionary *firstBasePoint = [objects objectNamed:@"FirstBasePoint"];
//    NSAssert(firstBasePoint != nil, @"FirstBasePoint object not found");
//    NSMutableDictionary *secondBasePoint = [objects objectNamed:@"SecondBasePoint"];
//    NSAssert(secondBasePoint != nil, @"SecondBasePoint object not found");
//    
//    player1.startPoint = ccp([[firstPlayerStartPoint valueForKey:@"x"] intValue],[[firstPlayerStartPoint valueForKey:@"y"] intValue]);
//    player2.startPoint = ccp([[secondPlayerStartPoint valueForKey:@"x"] intValue],[[secondPlayerStartPoint valueForKey:@"y"] intValue]);
//    [player1 teleportToPosition:player1.startPoint];
//    [player2 teleportToPosition:player2.startPoint];
//    _PvPLogic.firstPlayerSpawnPoint = player1.startPoint;
//    _PvPLogic.secondPlayerSpawnPoint = player2.startPoint;
//    base1.position = ccp([[firstBasePoint valueForKey:@"x"] intValue], [[firstBasePoint valueForKey:@"y"] intValue]);
//    base2.position = ccp([[secondBasePoint valueForKey:@"x"] intValue], [[secondBasePoint valueForKey:@"y"] intValue]);
//    
//    _firstBaseLabel.position = ccp(base1.position.x + 20, base1.position.y + 20);
//    _secondBaseLabel.position = ccp(base2.position.x - 20, base2.position.y - 20);
//    
//    NSLog(@"After reading objects we have got:");
//    NSLog(@"player1.startPoint x%f, y%f",player1.startPoint.x,player1.startPoint.y);
//    NSLog(@"player2.startPoint x%f, y%f",player2.startPoint.x,player2.startPoint.y);
//    NSLog(@"base1.position x%f,y%f",base1.position.x,base1.position.y);
//    NSLog(@"base2.position x%f,y%f",base2.position.x,base2.position.y);
    
}

-(void)initPlayers
{
    
    CGSize winSize = [CCDirector sharedDirector].winSize;
    
    player1 = [[[Player alloc] initWithLoader:[[SHPlayerManager sharedSHPlayerManager] getFirstPlayerLoader]] autorelease];
//    player1.startPoint = ccp(winSize.width*-20, winSize.height*-20); // hide em
    player1.startPoint = ccp(_tileMapObjectsInfo.firstPlayerStartPoint.x, _tileMapObjectsInfo.firstPlayerStartPoint.y);
    [self addChild:player1 z:10];
    
    p1controller = [[[[PlayerController alloc] initWithPlayer:player1 
                                                atCenterPoint:ccp(winSize.width*0.1,winSize.height*0.5) 
                                                   withMargin:(winSize.width*0.27) 
                                                   isVertical:YES 
                                                   isReversed:NO] autorelease] retain];
    [_HUDLayer addChild:p1controller];
    
    
    player2 = [[[Player alloc] initWithLoader:[[SHPlayerManager sharedSHPlayerManager] getSecondPlayerLoader]] autorelease];
//    player2.startPoint = ccp(winSize.width*-10, winSize.height*-10); // hide em
    player2.startPoint = ccp(_tileMapObjectsInfo.secondPlayerStartPoint.x, _tileMapObjectsInfo.secondPlayerStartPoint.y);
    [self addChild:player2 z:10];
    
    p2controller = [[[[PlayerController alloc] initWithPlayer:player2 
                                                atCenterPoint:ccp(winSize.width*0.9,winSize.height*0.5) 
                                                   withMargin:(winSize.width*0.27) 
                                                   isVertical:YES 
                                                   isReversed:YES] autorelease] retain];
    
    [_HUDLayer addChild:p2controller];
    
    // get the fireports
    NSMutableArray *p1fireports = [NSMutableArray new];
    NSMutableArray *p2fireports = [NSMutableArray new];
    [p1fireports addObject:[NSValue valueWithCGPoint:ccp(0,50)]];
    [p2fireports addObject:[NSValue valueWithCGPoint:ccp(0,50)]];
    
    p1shooter = [[[PlayerShooting alloc] initWithPlayerControlledBody:player1.body andLayer:self andPlayer:player1 andFireports:p1fireports] autorelease];
    [self addChild:p1shooter];
    p2shooter = [[[PlayerShooting alloc] initWithPlayerControlledBody:player2.body andLayer:self andPlayer:player2 andFireports:p2fireports] autorelease];
    [self addChild:p2shooter];
}

-(void)initLogic
{
    // init PvP logic and assign its HUD
    _PvPLogic = [SHPvPLogic sharedSHPvPLogic];
    _PvPLogic.myHUDLayer = _HUDLayer;
    
    _PvPLogic.firstPlayerSpawnPoint = _tileMapObjectsInfo.firstPlayerStartPoint;
    _PvPLogic.secondPlayerSpawnPoint = _tileMapObjectsInfo.secondPlayerStartPoint;
}

-(void)initBases
{
    base1 = [[[Base alloc] init] autorelease];
    base2 = [[[Base alloc] init] autorelease];
    [self addChild:base1 z: 1];
    [self addChild:base2 z: 1];
    _PvPLogic.firstBase = base1;
    _PvPLogic.secondBase = base2;
    
    _firstBaseLabel = [CCLabelTTF labelWithString:@"" fontName:@"Marker Felt" fontSize:20];
    _secondBaseLabel = [CCLabelTTF labelWithString:@"" fontName:@"Marker Felt" fontSize:20];
    _firstBaseLabel.color = ccc3(FIRST_BASE_LABEL_COLOR);
    _secondBaseLabel.color = ccc3(SECOND_BASE_LABEL_COLOR);
    [self addChild:_firstBaseLabel z:3];
    [self addChild:_secondBaseLabel z:3];
    _firstBaseLabel.position = ccp(_tileMapObjectsInfo.firstBasePoint.x, _tileMapObjectsInfo.firstBasePoint.y);
    _secondBaseLabel.position = ccp(_tileMapObjectsInfo.secondBasePoint.x, _tileMapObjectsInfo.secondBasePoint.y);
}
                                                            
-(void)initCollisionManager
{
    // Tuning new box2d collision manager
    _b2dColMan = [SHb2dCollisionManager sharedSHb2dCollisionManager];
    _b2dColMan.meta = _meta;
    _b2dColMan.tileMap = _tileMap;
    [_b2dColMan gatherCollidableTiles];
}

-(void)onEnter
{
    [super onEnter];
    [self readObjects];
    [self initLogic];
    [self initBases];
    [self initCollisionManager];
    [self initPlayers];
    [self schedule:@selector(b2dCollisionSimulation:) interval:1/60];
    self.scale = 1.0f;
}

-(void)b2dCollisionSimulation:(ccTime)dt
{
    [_b2dColMan tick:dt];
}

-(void)draw
{
    [super draw];
    [_b2dColMan drawDebug];
}

- (float)tileMapHeight {
    return _tileMap.mapSize.height * _tileMap.tileSize.height;
}

- (float)tileMapWidth {
    return _tileMap.mapSize.width * _tileMap.tileSize.width;
}

-(void)setViewpointCenter:(CGPoint) position {
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    position = ccpMult(position, self.scale);
    int x = MAX(position.x, winSize.width / 2 );
    int y = MAX(position.y, winSize.height / 2 );
    x = MIN(x, [self tileMapWidth] - winSize.width / 2 );
    y = MIN(y, [self tileMapHeight] - winSize.height/ 2 );
    CGPoint actualPosition = ccp(x, y);
    
    CGPoint centerOfView = ccpMult(ccp(winSize.width/2, winSize.height/2), self.scale) ;
    CGPoint viewPoint = ccpSub(centerOfView, actualPosition);
    
    self.position = viewPoint;
    [ViewportController sharedViewportController].centerOfViewportOnLayer = actualPosition;
}

-(void)update:(ccTime)dt;
{
    // now we've got nothing to do with them
    [_projectiles removeAllObjects];
    
    // center viewport;
    [self setViewpointCenter:[ViewportController sharedViewportController].centerOfViewport];
    

    // test add zombie
    if (_testZombieInhibitor == nil)
    {
        CGSize winSize = [CCDirector sharedDirector].winSize;
        _testZombieInhibitor = [[[SHZombieInhibitor alloc] initWithSpawnRate:0.8f andTargets:[NSArray arrayWithObjects:player1,player2, nil] andMaximumZombies:40 andLayer:self] autorelease];
        [self addChild:_testZombieInhibitor];
        _testZombieInhibitor.position = ccp(-200,200);
        id actionMove1 = [CCMoveTo actionWithDuration:2.0f position:ccp(0,0)];
        id actionMove2 = [CCMoveTo actionWithDuration:2.0f position:ccp(0,winSize.height)];
        id actionMove3 = [CCMoveTo actionWithDuration:2.0f position:ccp(winSize.width,winSize.height)];
        id actionMove4 = [CCMoveTo actionWithDuration:2.0f position:ccp(winSize.width,0)];
        id seq = [CCSequence actions:actionMove1, actionMove2, actionMove3, actionMove4, nil];
        [_testZombieInhibitor runAction:[CCRepeatForever actionWithAction:seq]];
    }
    
    
    // update base labels
    _firstBaseLabel.string = [NSString stringWithFormat:@"%i",_PvPLogic.firstBaseHealth.intValue];
    _secondBaseLabel.string = [NSString stringWithFormat:@"%i",_PvPLogic.secondBaseHealth.intValue];
//    NSLog(@"Base1 position: x%f,y%f",_firstBaseLabel.position.x,_firstBaseLabel.position.y);
//    NSLog(@"Base2 position: x%f,y%f",_secondBaseLabel.position.x,_secondBaseLabel.position.y);
    
    // check if we need to change timeScale
    if (_slowMoToggle)
    {
        _slowMoTimeAccum+=dt;
        if (_slowMoTimeAccum > 0.025)
        {
            float currentTimeScale = [self scheduler].timeScale;
            if (_slowMoTimeScaleTargetValue < _slowMoTimeScaleBeginValue) // so we are slowing down the motion
            {
                [self scheduler].timeScale -= (_slowMoTimeScaleBeginValue - currentTimeScale)/3+0.01;
                if ([self scheduler].timeScale <= _slowMoTimeScaleTargetValue)
                {
                    // we finished slowing
                    [self scheduler].timeScale = _slowMoTimeScaleTargetValue;
                    _slowMoToggle = NO;
                }
            }
            else if (_slowMoTimeScaleTargetValue > _slowMoTimeScaleBeginValue) // so we are speeding up the motion
            {
                [self scheduler].timeScale += (currentTimeScale - _slowMoTimeScaleBeginValue)/3+0.02;
                if ([self scheduler].timeScale >= _slowMoTimeScaleTargetValue)
                {
                    // we finished speeding
                    [self scheduler].timeScale = _slowMoTimeScaleTargetValue;
                    _slowMoToggle = NO;
                }
            }
            _slowMoTimeAccum = 0;
        }
    }
    else
    {
        _slowMoTimeAccum = 0;
    }
}

-(void)doSlowMo
{
    _slowMoTimeScaleTargetValue = 0.2f;
    _slowMoTimeScaleBeginValue = [self scheduler].timeScale;
    self.isOnSlowMo = YES;
    _slowMoToggle = YES;
    
}

-(void)undoSlowMo
{
    _slowMoTimeScaleTargetValue = 1.0f;
    _slowMoTimeScaleBeginValue = [self scheduler].timeScale;
    _slowMoToggle = YES;
}

-(void)doChangeScale:(float)incScale
{
    self.scale = incScale;
    [ViewportController sharedViewportController].layerScale = self.scale;
    float h = [self tileMapHeight];
    float multH = incScale * h;
    float diffH = h - multH;
    [ViewportController sharedViewportController].addingOnScaleY = diffH /2;
    
    float w = [self tileMapWidth];
    float multW = incScale * w;
    float diffW = w - multW;
    [ViewportController sharedViewportController].addingOnScaleX = diffW /2;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    self.myHaloLayer = nil;
    [_projectiles release];
    self.tileMap = nil;
    self.background = nil;
    self.objects = nil;
    self.meta = nil;
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}
@end
