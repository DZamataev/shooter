//
//  SHPlayerMovementDelegate.h
//  Anti-heroes Wars
//
//  Created by Denis on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SHPlayerMovementDelegate <NSObject>
-(void)movingStatusChangedWithBool:(bool)isM;
@end
