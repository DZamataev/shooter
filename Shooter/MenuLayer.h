//
//  MenuLayer.h
//  SomeShooter
//
//  Created by Denis on 3/17/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameplayLayer.h"
#import "TestLayer.h"

@interface MenuLayer : CCLayer {
    
}
+(CCScene*) scene;


-(void)switchToGameplay;
-(void)switchToTest;
-(void)enableMusic;
@end
