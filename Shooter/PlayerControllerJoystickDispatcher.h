//
//  PlayerControllerJoystickDispatcher.h
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZJoystick.h"

@protocol PlayerControllerJoystickDispatcherDelegate

-(void)joystickControlBeganWithTag:(int)joyTag;
-(void)joystickControlDidUpdate:(id)joystick toXSpeedRatio:(CGFloat)xSpeedRatio toYSpeedRatio:(CGFloat)ySpeedRatio withTag:(int)joyTag;
-(void)joystickControlEndedWithTag:(int)joyTag;
-(void)joystickControlMovedWithTag:(int)joyTag;
@end

@interface PlayerControllerJoystickDispatcher : NSObject <ZJoystickDelegate>
{
    int _tag;
    id<PlayerControllerJoystickDispatcherDelegate> delegate;
}
@property int tag;

-(id)initWithTag:(int)incTag andDelegate:(id<PlayerControllerJoystickDispatcherDelegate>)del;
@end
