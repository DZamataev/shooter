//
//  SDrawableLayer.h
//  Shooter
//
//  Created by Sergey on 3/27/12.
//  Copyright (c) 2012 DevitGroup. All rights reserved.
//

// Notes:
//
// Tested only with sprites
//
// To use as background, just add background sprite as 
// children after initiation
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface SDrawableLayer : CCLayer {
    int childrenCntUntilDraw;
    ccTime timeCntUntilDraw;
    ccColor4F clearColor;
    ccTime timeAccum;
    float fadeSpeed;
    bool cleanChilden;
    
    
    CCSprite *background;
}
@property (assign) int childrenCntUntilDraw;
@property (assign) ccTime timeCntUntilDraw;
@property (assign) ccColor4F clearColor;
@property (assign) float fadeSpeed;
@property (assign) bool cleanChilden;

-(void)drawAllChildrenWithCleanup:(BOOL)cleanup;
-(CCSprite*)drawAllChildren;
-(void)update:(ccTime)dt;
-(void)switchAutoCleanTo:(BOOL)isAutoClean;

+(SDrawableLayer*)layerWithPlume;
+(SDrawableLayer*)layerWithAutoclean;
@end
