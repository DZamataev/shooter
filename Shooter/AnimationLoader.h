//
//  AnimationLoader.h
//  KnightLauncher
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CCAnimate+SequenceLoader.h"
#import "CCAnimation+SequenceLoader.h"

@interface AnimationLoader : NSObject
+(CCAnimation*)loadPlistForAnimationWithName:(NSString*)animationName
                                andClassName:(NSString*)className;
+(CCAnimate*)loadAnimationAnimateForMatch3WithName:(NSString*)animationName 
                                            andTypeName:(NSString*)typeName 
                                           andClassName:(NSString*)className;
+(CCAnimation*)oldStyleLoadPlistForAnimationWithName:(NSString*)animationName
                                        andClassName:(NSString*)className;
/**
 * Loads animation frames into an array of CCSpriteFrame objects
 * @param nameFormat name format for the sprites to load, e.g. sprite_%02d
 * @param numFrames number of frames to load
 */
NSMutableArray* loadAnimFrames(NSString *nameFormat, int numFrames)
{
    CCSpriteFrameCache *cache = [CCSpriteFrameCache sharedSpriteFrameCache];
    NSMutableArray *animFrames = [NSMutableArray arrayWithCapacity:numFrames];
    for(int i = 1; i <= numFrames; i++)
    {
        [animFrames addObject:
         [cache spriteFrameByName:
          [NSString stringWithFormat:nameFormat,i]]];
    }
    return animFrames;
}

/**
 * Loads an animation
 * @param nameFormat name format for the sprites to load, e.g. sprite_%02d
 * @param numFrames number of frames to load
 * @param delay time between frames
 */
CCAnimation* loadAnimSequence(NSString *nameFormat, int numFrames, float delay)
{
    return [CCAnimation 
            animationWithFrames:loadAnimFrames(nameFormat, numFrames)
            delay:delay];
}

/**
 * Loads an animation as usable action
 * @param nameFormat name format for the sprites to load, e.g. sprite_%02d
 * @param numFrames number of frames to load
 * @param delay time between frames
 * @param restoreOriginalFrame restores the original frame at the end of the animation
 */
CCAnimate* loadAnimAction(NSString *nameFormat, int numFrames, float delay, BOOL restoreOriginalFrame)
{
    return [CCAnimate 
            actionWithAnimation:loadAnimSequence(nameFormat, numFrames, delay) 
            restoreOriginalFrame:restoreOriginalFrame];
}


@end
