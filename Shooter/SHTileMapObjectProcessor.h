//
//  SHTileMapObjectProcessor.h
//  Anti-heroes Wars
//
//  Created by Denis on 4/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

struct SHTileMapObjectsInfo {
    CGPoint firstPlayerStartPoint;
    CGPoint secondPlayerStartPoint;
    CGPoint firstBasePoint;
    CGPoint secondBasePoint;
};

@interface SHTileMapObjectProcessor : NSObject
{
}
+(struct SHTileMapObjectsInfo)getInfoForTileMap:(CCTMXTiledMap*)tileMap withObjectsLayerNamed:(NSString*)objectsLayerName;
@end
