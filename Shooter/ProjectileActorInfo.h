//
//  ProjectileActorInfo.h
//  Shooter
//
//  Created by Denis on 3/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameActorInfo.h"

@interface ProjectileActorInfo : GameActorInfo
{
    GameActorInfo *_collidedObject;
    int _damage;
    bool _impulseApplied;
    int _bounces;
    int _penetration;
    NSMutableArray *_penetrated;
    NSMutableArray *_contacted;
}

@property (nonatomic, assign) GameActorInfo *collidedObject;
@property int damage;
@property bool impulseApplied;
@property int bounces;
@property int penetration;
-(id)initWithPosition:(CGPoint)pos andRotation:(CGFloat)rotation andRect:(CGRect)rect andGotType:(gameObjectType)got andAttType:(attachedToType)att;

-(void)pushPenetratedInfo:(GameActorInfo*)info;
-(void)pushContact:(GameActorInfo*)info;
-(bool)actorAlreadyPenetrated:(GameActorInfo*)info;
-(bool)actorAlreadyContacted:(GameActorInfo*)info;
-(void)eraseContacted:(GameActorInfo*)info;
@end
