//
//  Situation.m
//  Anti-heroes Wars
//
//  Created by Denis on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Situation.h"

@implementation Situation
CWL_SYNTHESIZE_SINGLETON_FOR_CLASS(Situation)
@synthesize tileSizeDisplayMultiplier = _tileSizeDisplayMultiplier;
@end
