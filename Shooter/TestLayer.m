//
//  TestLayer.m
//  SomeShooter
//
//  Created by Denis on 3/17/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "TestLayer.h"


@implementation TestLayer
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	TestLayer *layer = [TestLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}
-(id)init
{
    self = [super init];
    
    return self;
}
@end
