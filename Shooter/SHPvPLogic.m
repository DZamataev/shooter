//
//  SHPvPLogic.m
//  Anti-heroes Wars
//
//  Created by Denis on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SHPvPLogic.h"
#import "HUDLayer.h"
#import "Player.h"
#import "SHPlayerHealth.h"
#import "Base.h"
#import "SHBaseHealth.h"

@implementation SHPvPLogic
@synthesize firstPlayer = _firstPlayer;
@synthesize secondPlayer = _secondPlayer;
@synthesize firstPlayerHealth = _firstPlayerHealth;
@synthesize secondPlayerHealth = _secondPlayerHealth;
@synthesize firstBase = _firstBase;
@synthesize secondBase = _secondBase;
@synthesize firstBaseHealth = _firstBaseHealth;
@synthesize secondBaseHealth = _secondBaseHealth;
@synthesize firstPlayerSpawnPoint = _firstPlayerSpawnPoint;
@synthesize secondPlayerSpawnPoint = _secondPlayerSpawnPoint;
CWL_SYNTHESIZE_SINGLETON_FOR_CLASS(SHPvPLogic)

-(id)init
{
    self = [super init];
    _firstPlayer = _secondPlayer = nil;
    _firstBase = _secondBase = nil;
    _firstPlayerHealth = [SHPlayerHealth SHPlayerHealthWithInt:25];
    _secondPlayerHealth = [SHPlayerHealth SHPlayerHealthWithInt:25];
    _firstBaseHealth = [SHBaseHealth SHBaseHealthWithInt:100];
    _secondBaseHealth = [SHBaseHealth SHBaseHealthWithInt:100];
    return self;
}

-(void)setMyHUDLayer:(HUDLayer *)myHUDLayer
{
    _myHUDLayer = myHUDLayer;
    _myHUDLayer.firstPlayerHealth = _firstPlayerHealth;
    _myHUDLayer.secondPlayerHealth = _secondPlayerHealth;
}

-(HUDLayer*)myHUDLayer
{
    return _myHUDLayer;
}

#pragma mark -
#pragma mark Player logic

-(Player*)opponentPlayerFor:(Player *)player
{
    Player* result = nil;
    if (player == _firstPlayer)
        result = _secondPlayer;
    else
        result = _firstPlayer;
    return result;
}

-(SHPlayerHealth*)healthCounterForPlayer:(Player *)player
{
    SHPlayerHealth *result = nil;
    if (player == _secondPlayer)
        result = _secondPlayerHealth;
    else 
        result = _firstPlayerHealth;
    return result;
}

-(void)player:(Player *)player gotHitByProjectile:(ProjectileActorInfo*)projInfo
{
    [self dealDamage:projInfo.damage toPlayer:player];
}

-(void)player:(Player *)player hitsOpponentPlayerWithProjectileInfo:(ProjectileActorInfo *)projInfo
{
    Player *opponentPlayer = [self opponentPlayerFor:player];
    [self dealDamage:projInfo.damage toPlayer:opponentPlayer];
}

-(void)dealDamage:(int)dmg toPlayer:(Player*)player
{
    SHPlayerHealth *playerHealth = [self healthCounterForPlayer:player];
    if (dmg >= playerHealth.intValue)
    {
        // damage will kill the player
        [self reportDeathFor:player];
    }
    else
    {
        // deal damage on health
        playerHealth.intValue -= dmg;
    }
}


-(void)reportDeathFor:(Player *)player
{
    CGPoint spawnLocation = [self spawnPointForPlayer:player];
    [player teleportToPosition:spawnLocation];
    SHPlayerHealth *playerHealth = [self healthCounterForPlayer:player];
    playerHealth.intValue = 25;
}

-(CGPoint)spawnPointForPlayer:(Player *)player
{
    CGPoint result = ccp(-1000,-1000);
    if (player == _firstPlayer)
        result = _firstPlayerSpawnPoint;
    else {
        result = _secondPlayerSpawnPoint; 
    }
    return result;
}
#pragma mark -
#pragma mark Base Logic


-(SHBaseHealth*)healthCounterForBase:(Base*)base
{
    SHBaseHealth *result = nil;
    if (base == _secondBase)
        result = _secondBaseHealth;
    else 
        result = _firstBaseHealth;
    return result;
}

-(void)base:(Base *)base gotHitByProjectile:(ProjectileActorInfo *)projInfo
{
    [self dealDamage:projInfo.damage toBase:base];
}

-(void)dealDamage:(int)dmg toBase:(Base *)base
{
    SHBaseHealth *baseHealth = [self healthCounterForBase:base];
    if (dmg >= baseHealth.intValue)
    {
        // damage will destroy the base
        [self reportDestructionFor:base];
    }
    else
    {
        // deal damage on health
        baseHealth.intValue -= dmg;
    }
}

-(void)reportDestructionFor:(Base *)base
{
    
}


@end
