//
//  SHb2dCollisionManager.h
//  Shooter
//
//  Created by Denis on 3/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CWLSynthesizeSingleton.h"
#import "cocos2d.h"
#import "Box2D.h"
#import "GLES-Render.h"
#import "MyContactListener.h"

#import "GameActorInfo.h"
#import "PlayerActorInfo.h"
#import "ProjectileActorInfo.h"

#define PTM_RATIO 32

#define CATEGORY_BOUNDRY 0x0001
#define CATEGORY_PLAYER 0x0002 
#define CATEGORY_MONSTER 0x0004 
#define CATEGORY_PROJECTILE 0x0008 

#define MASK_PLAYER 

struct SHBodyInitType {
    // logical
    
    bool isPlayer;
    bool isBase;
    
    // physical
    b2BodyType btype;
    bool isSensor;
    bool isBullet;
    float linearDamping;
    float restitution;
    uint16 categoryBits;
    uint16 maskBits;
};

typedef enum {tstLDC = 0,tstLUC,tstRUC,tstRDC} SHTriangleShapeType;

@interface SHb2dCollisionManager : NSObject
{
    CCTMXTiledMap *_tileMap;
    CCTMXLayer *_meta;
    NSArray *_collidableTiles;
    b2World *_world;
    b2Body *_mapBoundsBody;
    b2Body *_viewportBoundsBody;
    GLESDebugDraw *_debugDraw;
    MyContactListener *_contactListener;
    NSMutableArray *_players;
    NSMutableArray *_bases;
}
@property (nonatomic, retain) CCTMXTiledMap *tileMap;
@property (nonatomic, retain) CCTMXLayer *meta;
@property (nonatomic,assign) b2World *world;
CWL_DECLARE_SINGLETON_FOR_CLASS(SHb2dCollisionManager)
-(void)gatherCollidableTiles;
-(void) createMapBounds:(CGSize)size;
-(void) createViewportBounds:(CGRect)rect withMargin:(float)marg;
-(void) updateViewportRect:(CGSize)size withMargin:(float)marg;
-(void)tick:(ccTime)dt;
-(void)drawDebug;
-(void)addBoxBodyWithCircleShapeForGameObjectWithInfo:(GameActorInfo *)info;
-(void)addBoxBodyWithPolygonShapeForGameObjectWithInfo:(GameActorInfo *)info;
-(void)addBoxBodyWithPolygonShapeForGameObjectWithInfo:(GameActorInfo *)info with:(SHTriangleShapeType)tstType;

-(void)manageContactWithActorInfo:(GameActorInfo*)infoA andActorInfo:(GameActorInfo*)infoB;
-(void)manageContactWithDynamicActorInfo:(GameActorInfo*)dynInfo 
                      andStaticActorInfo:(GameActorInfo*)staticInfo;
-(void)manageContactWithDynamicActorInfo:(GameActorInfo*)dynInfoA 
                      andDynamicActorInfo:(GameActorInfo*)dynInfoB;

// shooter spectific methods

-(void)reportPossibleSafePointsForPlayers;

-(struct SHBodyInitType)switchInitTypeWithGOT:(gameObjectType)got andATT:(attachedToType)att;

// Non box2d collision tests

-(bool)rect:(CGRect)rect intersectsCircleWithOrigin:(CGPoint)p1 andRadius:(CGFloat)rect;
-(CGRect)calculateRectForPosition:(CGPoint)position andSize:(CGSize)size;
-(bool)rectCollisionTestForActorWithInfo:(GameActorInfo*)info1 andActorWithInfo:(GameActorInfo*)info2;

@end
