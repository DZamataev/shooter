//
//  SHZombieBehaviour.h
//  Anti-heroes Wars
//
//  Created by Denis on 4/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SHZombie;

@interface SHZombieBehaviour : NSObject
{
    NSMutableArray *_targets;
    NSMutableArray *_zombies;
    int _aliveZombies;
}
@property (nonatomic, readonly) int aliveZombies;
-(id)initWithTargets:(NSArray*)t;
-(void)addZombie:(SHZombie*)zom;
-(void)updateVelocities;
@end
