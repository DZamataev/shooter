//
//  SHBaseHealth.h
//  Anti-heroes Wars
//
//  Created by Denis on 4/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SHBaseHealth : NSObject
{
    int _intValue;
}
@property int intValue;
+(id)SHBaseHealthWithInt:(int)value;
@end
