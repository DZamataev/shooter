//
//  SHMonsterTargetProtocol.h
//  Anti-heroes Wars
//
//  Created by Denis on 4/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SHMonsterTargetProtocol <NSObject>
-(CGPoint)getTargetPosition;
@end
