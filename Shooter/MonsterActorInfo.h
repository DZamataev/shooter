//
//  MonsterActorInfo.h
//  Anti-heroes Wars
//
//  Created by Denis on 4/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameActorInfo.h"

@interface MonsterActorInfo : GameActorInfo
{
    CGPoint _forcedPosition; // used to reposition monster not by physics and forces physics to think its real position
    NSMutableArray *_contactsInfo;
}

@property CGPoint forcedPosition;
@property (nonatomic,retain) NSMutableArray *contactsInfo;

-(void)pushContact:(GameActorInfo*)contactInfo;
-(void)removePhysicsBody;
@end
