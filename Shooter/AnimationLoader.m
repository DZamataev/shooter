//
//  AnimationLoader.m
//  KnightLauncher
//

#import "AnimationLoader.h"

@implementation AnimationLoader

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

+(CCAnimation*)oldStyleLoadPlistForAnimationWithName:(NSString*)animationName
                                andClassName:(NSString*)className 
{
    CCAnimation *animationToReturn = nil;
    NSString *fullFileName =
    [NSString stringWithFormat:@"%@.plist",className];
    NSString *plistPath;
    
    // 1: Get the Path to the plist file
    NSString *rootPath =
    [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                         NSUserDomainMask, YES) objectAtIndex:0];
    plistPath = [rootPath stringByAppendingPathComponent:fullFileName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        plistPath = [[NSBundle mainBundle]
                     pathForResource:className ofType:@"plist"];
    }
    
    // 2: Read in the plist file
    NSDictionary *plistDictionary =
    [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
    // 3: If the plistDictionary was null, the file was not found.
    if (plistDictionary == nil) {
        CCLOG(@"Error reading plist: %@.plist", className);
        return nil; // No Plist Dictionary or file found
    }
    
    if ([plistDictionary objectForKey:animationName] == nil) {
        CCLOG(@"Could not locate AnimationWithName:%@",animationName);
        return nil; 
    }
    
    // 5: Get the delay value for the animation
    float animationDelay =
    [[plistDictionary objectForKey:@"FrameDelay"] floatValue];
    
    animationToReturn = [CCAnimation animation];
    [animationToReturn setDelay:animationDelay]; 
    
    
    // 6: Add the frames to the animation
    NSArray *animationFrames =
    [plistDictionary objectForKey:animationName];
    for (NSString *frameName in animationFrames) {
        [animationToReturn addFrameWithFilename:frameName];
    }
    return animationToReturn;
}


+(CCAnimation*)loadPlistForAnimationWithName:(NSString*)animationName
                                andClassName:(NSString*)className 
{
    CCAnimation *animationToReturn = nil;
    NSString *fullFileName =
    [NSString stringWithFormat:@"%@.plist",className];
    NSString *plistPath;
    
    // 1: Get the Path to the plist file
    NSString *rootPath =
    [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                         NSUserDomainMask, YES) objectAtIndex:0];
    plistPath = [rootPath stringByAppendingPathComponent:fullFileName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        plistPath = [[NSBundle mainBundle]
                     pathForResource:className ofType:@"plist"];
    }
    
    // 2: Read in the plist file
    NSDictionary *plistDictionary =
    [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
    // 3: If the plistDictionary was null, the file was not found.
    if (plistDictionary == nil) {
        CCLOG(@"Error reading plist: %@.plist", className);
        return nil; // No Plist Dictionary or file found
    }
    
    if ([plistDictionary objectForKey:animationName] == nil) {
        CCLOG(@"Could not locate AnimationWithName:%@",animationName);
        return nil; 
    }
    
    // 4: Get the animation dictionary
    NSDictionary *animationDictionary = [plistDictionary objectForKey:animationName];
    
    // 5: Get the parameters
    float frameDelay = [[animationDictionary objectForKey:@"frameDelay"] floatValue];
    
    NSString *animationFormat = nil;
    animationFormat = [animationDictionary objectForKey:@"animationFormat"];
    
    NSString *datafileName = nil;
    datafileName =  [animationDictionary objectForKey:@"datafile"];
    
    int numFrames = [[animationDictionary objectForKey:@"numFrames"] intValue];
    
    if (animationFormat == nil || datafileName == nil)
    {
        CCLOG(@"Could not locate spritesheet name (%@) or datafile name (%@)",animationFormat,datafileName);
        return nil; 
    }
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:datafileName];
    
    animationToReturn = [CCAnimation animationWithSpriteSequence:animationFormat numFrames:numFrames delay:frameDelay];
    
    return animationToReturn;
}

+(CCAnimate*)loadAnimationAnimateForMatch3WithName:(NSString*)animationName 
                                            andTypeName:(NSString*)typeName 
                                           andClassName:(NSString*)className 
{
    CCAnimate *animateActionToReturn = nil;
    NSString *fullFileName =
    [NSString stringWithFormat:@"match3/%@.plist",className];
    NSString *plistPath;
    
    // 1: Get the Path to the plist file
    NSString *rootPath =
    [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                         NSUserDomainMask, YES) objectAtIndex:0];
    plistPath = [rootPath stringByAppendingPathComponent:fullFileName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        plistPath = [[NSBundle mainBundle]
                     pathForResource:className ofType:@"plist"];
    }
    
    // 2: Read in the plist file
    NSDictionary *plistDictionary =
    [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
    // 3: If the plistDictionary was null, the file was not found.
    if (plistDictionary == nil) {
        CCLOG(@"Error reading plist: %@.plist", className);
        return nil; // No Plist Dictionary or file found
    }
    
    if ([plistDictionary objectForKey:typeName] == nil) {
        CCLOG(@"Could not locate Type:%@",typeName);
        return nil; 
    }
    
    // 4: Get dictionary for my type
    NSDictionary *typeDictionary = [plistDictionary objectForKey:typeName];
    
    // 4.1 Get dictionary for my animation name
    if ([typeDictionary objectForKey:animationName] == nil) {
        CCLOG(@"Could not locate Animation:%@",animationName);
        return nil; 
    }
    NSDictionary *animationDictionary = [typeDictionary objectForKey:animationName];
    
    // 5: Get the parameters
    float frameDelay = [[animationDictionary objectForKey:@"frameDelay"] floatValue];
    
    NSString *animationFormat = nil;
    animationFormat = [animationDictionary objectForKey:@"animationFormat"];
    
    NSString *datafileName = nil;
    datafileName =  [animationDictionary objectForKey:@"datafile"];
    
    int numFrames = [[animationDictionary objectForKey:@"numFrames"] intValue];
    
    if (animationFormat == nil || datafileName == nil)
    {
        CCLOG(@"Could not locate spritesheet name (%@) or datafile name (%@)",animationFormat,datafileName);
        return nil; 
    }
    
   
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:datafileName];
    

    animateActionToReturn = [CCAnimate actionWithSpriteSequence:animationFormat numFrames:numFrames delay:frameDelay restoreOriginalFrame:NO];
    
    return animateActionToReturn;
}

    
@end
