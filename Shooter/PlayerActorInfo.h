//
//  PlayerActorInfo.h
//  Shooter
//
//  Created by Denis on 3/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameActorInfo.h"
#import "cocos2d.h"

@interface PlayerActorInfo : GameActorInfo
{
    CGPoint _forcedPosition; // used to reposition player not by physics and force physics to think its real position
    CGPoint _safePoint;
    bool _collisionWorkedOutAndSolved;
    NSMutableArray *_contactsInfo;
}

@property CGPoint forcedPosition;
@property CGPoint safePoint;
@property bool collisionWorkedOutAndSolved;
@property (nonatomic,retain) NSMutableArray *contactsInfo;

-(void)pushContact:(GameActorInfo*)contactInfo;
@end
