//
//  SHTileMapObjectProcessor.m
//  Anti-heroes Wars
//
//  Created by Denis on 4/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SHTileMapObjectProcessor.h"

@implementation SHTileMapObjectProcessor
+(struct SHTileMapObjectsInfo)getInfoForTileMap:(CCTMXTiledMap *)tileMap withObjectsLayerNamed:(NSString *)objectsLayerName
{
    struct SHTileMapObjectsInfo result;
    CCTMXObjectGroup *objects = [tileMap objectGroupNamed:objectsLayerName];
    NSAssert(objects != nil, @"'Objects' object group not found");
    NSMutableDictionary *firstPlayerStartPointDict = [objects objectNamed:@"FirstPlayerStartPoint"];        
    NSAssert(firstPlayerStartPointDict != nil, @"FirstPlayerStartPoint object not found");
    NSMutableDictionary *secondPlayerStartPointDict = [objects objectNamed:@"SecondPlayerStartPoint"];        
    NSAssert(secondPlayerStartPointDict != nil, @"SecondPlayerStartPoint object not found");
    NSMutableDictionary *firstBasePointDict = [objects objectNamed:@"FirstBasePoint"];
    //NSAssert(firstBasePointDict != nil, @"FirstBasePoint object not found");
    NSMutableDictionary *secondBasePointDict = [objects objectNamed:@"SecondBasePoint"];
    //NSAssert(secondBasePointDict != nil, @"SecondBasePoint object not found");
    
    result.firstPlayerStartPoint = ccp([[firstPlayerStartPointDict valueForKey:@"x"] intValue],[[firstPlayerStartPointDict valueForKey:@"y"] intValue]);
    result.secondPlayerStartPoint = ccp([[secondPlayerStartPointDict valueForKey:@"x"] intValue],[[secondPlayerStartPointDict valueForKey:@"y"] intValue]);
    result.firstBasePoint = ccp([[firstBasePointDict valueForKey:@"x"] intValue], [[firstBasePointDict valueForKey:@"y"] intValue]);
    result.secondBasePoint = ccp([[secondBasePointDict valueForKey:@"x"] intValue], [[secondBasePointDict valueForKey:@"y"] intValue]);
    
    NSLog(@"After processing objects we have got:");
    NSLog(@"player1.startPoint x%f, y%f",result.firstPlayerStartPoint.x,result.firstPlayerStartPoint.y);
    NSLog(@"player2.startPoint x%f, y%f",result.secondPlayerStartPoint.x,result.secondPlayerStartPoint.y);
    NSLog(@"base1.position x%f,y%f",result.firstBasePoint.x,result.firstBasePoint.y);
    NSLog(@"base2.position x%f,y%f",result.secondBasePoint.y,result.secondBasePoint.y);
    return result;
}
@end
