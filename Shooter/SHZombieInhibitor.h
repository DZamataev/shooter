//
//  SHZombieInhibitor.h
//  Anti-heroes Wars
//
//  Created by Denis on 4/9/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class SHZombie;
@class SHZombieBehaviour;

@interface SHZombieInhibitor : CCNode {
    SHZombieBehaviour *_behaviour;
    float _timeAccum;
    float _spawnRate;
    CCLayer *_layer;
    int _maximumZombies;
}

@property (nonatomic, retain) SHZombieBehaviour *behaviour;


-(id)initWithSpawnRate:(float)sr andTargets:(NSArray*)targets andMaximumZombies:(int)maxZomb andLayer:(CCLayer*)layer;

@end
