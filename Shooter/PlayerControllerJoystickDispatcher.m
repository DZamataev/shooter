//
//  PlayerControllerJoystickDispatcher.m
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PlayerControllerJoystickDispatcher.h"

@implementation PlayerControllerJoystickDispatcher
@synthesize tag = _tag;
-(id)initWithTag:(int)incTag andDelegate:(id<PlayerControllerJoystickDispatcherDelegate>)del
{
    self = [super init];
    _tag = incTag;
    delegate = del;
    return self;
}

-(void)joystickControlBegan
{
    [delegate joystickControlBeganWithTag:_tag];
}

-(void)joystickControlDidUpdate:(id)joystick toXSpeedRatio:(CGFloat)xSpeedRatio toYSpeedRatio:(CGFloat)ySpeedRatio
{
    [delegate joystickControlDidUpdate:joystick toXSpeedRatio:xSpeedRatio toYSpeedRatio:ySpeedRatio withTag:_tag];
}

-(void)joystickControlEnded
{
    [delegate joystickControlEndedWithTag:_tag];
}

-(void)joystickControlMoved
{
    [delegate joystickControlMovedWithTag:_tag];
}

@end
