//
//  Base.m
//  Anti-heroes Wars
//
//  Created by Denis on 4/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Base.h"
#import "BaseActorInfo.h"
#import "SHPvPLogic.h"

@implementation Base
@synthesize health = _health;
@synthesize tilesInfo = _tilesInfo;

-(id)init
{
    self = [super init];
    _health = nil;
    _tilesInfo = [[NSMutableArray alloc] init];
    
    return self;
}

-(void)onEnter
{
    [super onEnter];
    [self scheduleUpdate];
}

-(void)pushTileInfo:(BaseActorInfo *)info
{
    [_tilesInfo addObject:info];
}

-(void)update:(ccTime)dt
{
    for (BaseActorInfo *baseInfo in _tilesInfo)
    {
        for (GameActorInfo *info in baseInfo.contactsInfo)
        {
            if (info.attType == attProjectile)
            {
                [[SHPvPLogic sharedSHPvPLogic] base:self gotHitByProjectile:(ProjectileActorInfo*)info];
            }
        }
        [baseInfo.contactsInfo removeAllObjects];
    }
}

-(void)dealloc
{
    [_tilesInfo release];
    [super dealloc];
}
@end
