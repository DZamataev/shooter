//
//  SHPlayerManager.h
//  Anti-heroes Wars
//
//  Created by Denis on 4/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CWLSynthesizeSingleton.h"

@class SHPlayerLoader;

@interface SHPlayerManager : NSObject
{
    
}
CWL_DECLARE_SINGLETON_FOR_CLASS(SHPlayerManager)
-(SHPlayerLoader*)getFirstPlayerLoader;
-(SHPlayerLoader*)getSecondPlayerLoader;
@end
