//
//  BaseActorInfo.h
//  Anti-heroes Wars
//
//  Created by Denis on 4/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameActorInfo.h"

@interface BaseActorInfo : GameActorInfo
{
    NSMutableArray *_contactsInfo;
}

@property (nonatomic,retain) NSMutableArray *contactsInfo;

-(void)pushContact:(GameActorInfo*)contactInfo;
@end
