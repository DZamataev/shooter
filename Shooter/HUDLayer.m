//
//  HUDLayer.m
//  Shooter
//
//  Created by Denis on 3/17/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "HUDLayer.h"
#import "GameplayLayer.h"
#import "SHPlayerHealth.h"

@implementation HUDLayer
@synthesize gameplayLayer = _gameplayLayer;
@synthesize firstPlayerHealth = _firstPlayerHealth;
@synthesize secondPlayerHealth = _secondPlayerHealth;
-(id)init
{
    self = [super init];
    CGSize winSize = [CCDirector sharedDirector].winSize;
    // zeroing
    _firstPlayerHealth = _secondPlayerHealth = nil;
    
    CCLabelTTF *slowMoLabel = [CCLabelTTF labelWithString:@"Slow-mo" fontName:@"Marker felt" fontSize:30.0f];
    slowMoLabel.color = ccc3(0, 0, 0);
    CCMenuItemLabel *slowMoBut = [CCMenuItemLabel itemWithLabel:slowMoLabel target:self selector:@selector(callSlowMo)];
    
//    CCLabelTTF *zoomInLabel = [CCLabelTTF labelWithString:@"+" fontName:@"Marker felt" fontSize:30.0f];
//    CCMenuItemLabel *zoomInBut = [CCMenuItemLabel itemWithLabel:zoomInLabel target:self selector:@selector(callZoomIn)];
//    
//    CCLabelTTF *zoomOutLabel = [CCLabelTTF labelWithString:@"-" fontName:@"Marker felt" fontSize:30.0f];
//    CCMenuItemLabel *zoomOutBut = [CCMenuItemLabel itemWithLabel:zoomOutLabel target:self selector:@selector(callZoomOut)];
    
    CCMenu *testMenu = [CCMenu menuWithItems:slowMoBut,/* zoomInBut, zoomOutBut,*/ nil];
    [self addChild:testMenu];
    [testMenu alignItemsVerticallyWithPadding:20.0f];
    testMenu.position = ccp(winSize.width*0.5, winSize.height*0.95);
    
    // init Player Health Display part
    _firstPlayerHealthLabel = [CCLabelTTF labelWithString:@"" fontName:@"Marker Felt" fontSize:20];
    _secondPlayerHealthLabel = [CCLabelTTF labelWithString:@"" fontName:@"Marker Felt" fontSize:20];
    _firstPlayerHealthLabel.color = _secondPlayerHealthLabel.color = ccc3(170, 20, 20);
    _firstPlayerHealthLabel.rotation = 90;
    _secondPlayerHealthLabel.rotation = -90;
    [self addChild:_firstPlayerHealthLabel];
    [self addChild:_secondPlayerHealthLabel];
    _firstPlayerHealthLabel.position = ccp(winSize.width*0.03f,winSize.height*0.5f);
    _secondPlayerHealthLabel.position = ccp(winSize.width*0.97f,winSize.height*0.5f);
    
    return self;
}

-(void)onEnter
{
    [super onEnter];
    [self scheduleUpdate];
    self.isTouchEnabled = YES;
}

-(void)callSlowMo
{
    if (!_gameplayLayer.isOnSlowMo)
    {
        [_gameplayLayer doSlowMo];
        _gameplayLayer.isOnSlowMo = YES;
    }
    else
    {
        [_gameplayLayer undoSlowMo];
        _gameplayLayer.isOnSlowMo = NO;
    }
}

-(void)callZoomIn
{
    [_gameplayLayer doChangeScale:_gameplayLayer.scale+0.05];
}

-(void)callZoomOut
{
    [_gameplayLayer doChangeScale:_gameplayLayer.scale-0.05];
}

-(void)update:(ccTime)dt
{
    // update data on Player Health Display
    _firstPlayerHealthLabel.string = [NSString stringWithFormat:@"%i",_firstPlayerHealth.intValue];
    _secondPlayerHealthLabel.string = [NSString stringWithFormat:@"%i",_secondPlayerHealth.intValue];
}
@end
