//
//  SHb2dCollisionManager.mm
//  Shooter
//
//  Created by Denis on 3/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SHb2dCollisionManager.h"
#import "SHMetaTile.h"
#import "ViewportController.h"
#import "SHBox2dBodyHolder.h"
#import "SHPvPLogic.h"
#import "Base.h"
#import "BaseActorInfo.h"
#import "MonsterActorInfo.h"

@implementation SHb2dCollisionManager
@synthesize world = _world;
@synthesize tileMap = _tileMap;
@synthesize meta = _meta;
CWL_SYNTHESIZE_SINGLETON_FOR_CLASS(SHb2dCollisionManager)

-(id)init
{
    self = [super init];
//    CGSize winSize = [CCDirector sharedDirector].winSize;
    b2Vec2 gravity = b2Vec2(0.0f, 0.0f);
    _world = new b2World(gravity);
    _world->SetAllowSleeping(false);
    _world->SetContinuousPhysics(true);
    
    // Create edges around the entire screen
//    b2BodyDef groundBodyDef;
//    groundBodyDef.position.Set(0,0);
//    _groundBody = _world->CreateBody(&groundBodyDef);
//    b2PolygonShape groundBox;
//    b2FixtureDef groundBoxDef;
//    groundBox.SetAsBox(winSize.width/PTM_RATIO, winSize.height/PTM_RATIO);
//    groundBoxDef.shape = &groundBox;
//    b2EdgeShape edge1;
//    edge1.Set(b2Vec2(0,0), b2Vec2(winSize.width/PTM_RATIO, 0));
    
    
     // Create contact listener
    _contactListener = new MyContactListener();
    _world->SetContactListener(_contactListener);
    
    // Enable debug draw
    _debugDraw = new GLESDebugDraw( PTM_RATIO );
    _world->SetDebugDraw(_debugDraw);
    
    uint32 flags = 0;
    flags += b2Draw::e_shapeBit;
    _debugDraw->SetFlags(flags);
    
    _players = [[NSMutableArray alloc] init];
    _bases = [[NSMutableArray alloc] init];
    
    return self;
}

-(void)gatherCollidableTiles
{
    NSMutableArray *collidableTiles = [NSMutableArray new];
    for (NSUInteger y=0; y<_meta.layerSize.height; y++)
    {
        for (NSUInteger x=0; x<_meta.layerSize.width; x++)
        {
            NSUInteger pos = x + _meta.layerSize.width * y;
            uint32_t tileGid = _meta.tiles[pos];
            if (tileGid) {
                NSDictionary *properties = [_tileMap propertiesForGID:tileGid];
                if (properties) {
                    NSString *collision = [properties valueForKey:@"Collidable"];
                    // if collidable property is "Full"
                    if ([collision isEqualToString:@"Full"]) {
                        
                        CGPoint tilePosition = ccp(x*_tileMap.tileSize.width+_tileMap.tileSize.width/2, 
                                                   ((_tileMap.mapSize.height - y) * _tileMap.tileSize.height) - _tileMap.tileSize.height/2);
                        SHMetaTile *tile = [[SHMetaTile alloc] initWithGid:tileGid andPosition:tilePosition];
                        [collidableTiles addObject:tile];
                        CGRect tileRect = CGRectMake(tilePosition.x-_tileMap.tileSize.width/2, 
                                                     tilePosition.y-_tileMap.tileSize.height/2, 
                                                     _tileMap.tileSize.width, _tileMap.tileSize.height);
                        
                        NSString *base = [properties valueForKey:@"Base"];
                        if (base)
                        {
                            BaseActorInfo *tileInfo;
                            // is a base tile. If we've got base tiles then its PvP so we can ask the logic for base.
                            if ([base isEqualToString:@"1"])
                            {
                                tileInfo = [[BaseActorInfo alloc] initWithPosition:tilePosition andRotation:0 andRect:tileRect andGotType:gotStatic andAttType:attBaseFirst];
                                if ([SHPvPLogic sharedSHPvPLogic].firstBase != nil)
                                {
                                    [[SHPvPLogic sharedSHPvPLogic].firstBase pushTileInfo:tileInfo];
                                }
                            }
                            else if ([base isEqualToString:@"2"])
                            {
                                tileInfo = [[BaseActorInfo alloc] initWithPosition:tilePosition andRotation:0 andRect:tileRect andGotType:gotStatic andAttType:attBaseSecond];
                                if ([SHPvPLogic sharedSHPvPLogic].secondBase != nil)
                                {
                                    [[SHPvPLogic sharedSHPvPLogic].secondBase pushTileInfo:tileInfo];
                                }
                            }
                            [self addBoxBodyWithPolygonShapeForGameObjectWithInfo:tileInfo];
                        }
                        else
                        {
                            // not a base tile. Just ordinary collidable tile!
                            GameActorInfo *tileInfo = [[GameActorInfo alloc] initWithPosition:tilePosition andRotation:0 andRect:tileRect andGotType:gotStatic andAttType:attCollidableTile];
                            [self addBoxBodyWithPolygonShapeForGameObjectWithInfo:tileInfo];
                        }
                    }
                    // if collidable property has something common with corners
                    else if ([collision isEqualToString:@"LDC"] || [collision isEqualToString:@"LUC"] || [collision isEqualToString:@"RUC"] || [collision isEqualToString:@"RDC"])
                    {
                        CGPoint tilePosition = ccp(x*_tileMap.tileSize.width+_tileMap.tileSize.width/2, 
                                                   ((_tileMap.mapSize.height - y) * _tileMap.tileSize.height) - _tileMap.tileSize.height/2);
                        
                        CGRect tileRect = CGRectMake(tilePosition.x-_tileMap.tileSize.width/2, 
                                                     tilePosition.y-_tileMap.tileSize.height/2, 
                                                     _tileMap.tileSize.width, _tileMap.tileSize.height);
                        // self array for tiles
                        SHMetaTile *tile = [[SHMetaTile alloc] initWithGid:tileGid andPosition:tilePosition];
                        [collidableTiles addObject:tile];
                        
                        GameActorInfo *tileInfo = [[GameActorInfo alloc] initWithPosition:tilePosition andRotation:0 andRect:tileRect andGotType:gotStatic andAttType:attCollidableTile];
                        
                        SHTriangleShapeType tsTypeOfTile = tstLDC;
                        if ([collision isEqualToString:@"LDC"])
                            tsTypeOfTile = tstLDC;
                        else if ([collision isEqualToString:@"LUC"])
                            tsTypeOfTile = tstLUC;
                        else if ([collision isEqualToString:@"RUC"])
                            tsTypeOfTile = tstRUC;
                        else if ([collision isEqualToString:@"RDC"])
                            tsTypeOfTile = tstRDC;
                        
                        [self addBoxBodyWithPolygonShapeForGameObjectWithInfo:tileInfo with:tsTypeOfTile];
                    }
                    
                    // if its spawn point
                    
                }
            }
        }
    }
    
    [_collidableTiles release];
    _collidableTiles = [[NSArray arrayWithArray:collidableTiles] retain];
    [collidableTiles release];
    CCLOG(@"Gather collidable tiles ended with count: %i",[_collidableTiles count]);
    
    // now we are ready to create ground bounds
    [self createMapBounds:CGSizeMake(_tileMap.mapSize.width*_tileMap.tileSize.width, _tileMap.mapSize.height*_tileMap.tileSize.height)];
    
}

-(void) createMapBounds:(CGSize)size
{
    
	float width = size.width;
	float height = size.height;
    
	float32 margin = 1.0f;
	b2Vec2 lowerLeft = b2Vec2(margin/PTM_RATIO, margin/PTM_RATIO);
	b2Vec2 lowerRight = b2Vec2((width-margin)/PTM_RATIO,margin/PTM_RATIO);
	b2Vec2 upperRight = b2Vec2((width-margin)/PTM_RATIO, (height-margin)/PTM_RATIO);
	b2Vec2 upperLeft = b2Vec2(margin/PTM_RATIO, (height-margin)/PTM_RATIO);
    
    GameActorInfo *mapBoundsInfo = [[GameActorInfo alloc] initWithPosition:ccp(width/2,height/2) andRotation:0 andRect:CGRectMake(0, 0, width, height) andGotType:gotStatic andAttType:attMapBounds];
    
	b2BodyDef mapBoundsBodyDef;
	mapBoundsBodyDef.type = b2_staticBody;
    mapBoundsBodyDef.userData = mapBoundsInfo;
	mapBoundsBodyDef.position.Set(0, 0);
	_mapBoundsBody = _world->CreateBody(&mapBoundsBodyDef);
    
	// Define the ground box shape.
	b2EdgeShape mapBoundsBox;		
    b2FixtureDef mapBoundsFixtureDef;
    mapBoundsFixtureDef.filter.categoryBits = CATEGORY_BOUNDRY;
    mapBoundsFixtureDef.filter.maskBits = CATEGORY_PLAYER | CATEGORY_PROJECTILE;
	// bottom
	mapBoundsBox.Set(lowerLeft, lowerRight);
    mapBoundsFixtureDef.shape = &mapBoundsBox;
	_mapBoundsBody->CreateFixture(&mapBoundsFixtureDef);
    
	// top
	mapBoundsBox.Set(upperRight, upperLeft);
    mapBoundsFixtureDef.shape = &mapBoundsBox;
	_mapBoundsBody->CreateFixture(&mapBoundsFixtureDef);
    
	// left
	mapBoundsBox.Set(upperLeft, lowerLeft);
    mapBoundsFixtureDef.shape = &mapBoundsBox;
	_mapBoundsBody->CreateFixture(&mapBoundsFixtureDef);
    
	// right
	mapBoundsBox.Set(lowerRight, upperRight);
    mapBoundsFixtureDef.shape = &mapBoundsBox;
	_mapBoundsBody->CreateFixture(&mapBoundsFixtureDef);
    
    CGRect rectt = [[ViewportController sharedViewportController] viewportRect];
    [self createViewportBounds:CGRectMake(0, 0, rectt.size.width, rectt.size.height) withMargin:0];
}

-(void) createViewportBounds:(CGRect)rect withMargin:(float)marg
{
    float width = rect.size.width;
	float height = rect.size.height;
    
    GameActorInfo *boundsInfo = [[GameActorInfo alloc] initWithPosition:ccp(rect.origin.x+rect.size.width/2,rect.origin.y+rect.size.height/2) andRotation: 0 andRect:rect andGotType:gotStatic andAttType:attViewportBounds];
    [ViewportController sharedViewportController].myInfo = boundsInfo;
    float x = rect.origin.x;
    float y = rect.origin.y;
    
	float32 margin = marg;
	b2Vec2 lowerLeft = b2Vec2((x-width/2+margin)/PTM_RATIO, (y-height/2 + margin)/PTM_RATIO);
	b2Vec2 lowerRight = b2Vec2((x+(width/2-margin))/PTM_RATIO,(y-height/2 +margin)/PTM_RATIO);
	b2Vec2 upperRight = b2Vec2((x+(width/2-margin))/PTM_RATIO, (y+(height/2-margin))/PTM_RATIO);
	b2Vec2 upperLeft = b2Vec2((x-width/2+margin)/PTM_RATIO, (y+(height/2-margin))/PTM_RATIO);
    
	b2BodyDef viewportBoundsBodyDef;
    viewportBoundsBodyDef.userData = boundsInfo;
	viewportBoundsBodyDef.type = b2_staticBody;
	viewportBoundsBodyDef.position.Set((x)/PTM_RATIO, (y)/PTM_RATIO);
	_viewportBoundsBody = _world->CreateBody(&viewportBoundsBodyDef);
    
	// Define the ground box shape.
	b2EdgeShape viewportBoundsBox;		
    b2FixtureDef viewportBoundsFixtureDef;
    viewportBoundsFixtureDef.filter.categoryBits = CATEGORY_BOUNDRY;
    viewportBoundsFixtureDef.filter.maskBits = CATEGORY_PLAYER | CATEGORY_PROJECTILE;
	// bottom
	viewportBoundsBox.Set(lowerLeft, lowerRight);
    viewportBoundsFixtureDef.shape = &viewportBoundsBox;
	_viewportBoundsBody->CreateFixture(&viewportBoundsFixtureDef);
    
	// top
	viewportBoundsBox.Set(upperRight, upperLeft);
    viewportBoundsFixtureDef.shape = &viewportBoundsBox;
	_viewportBoundsBody->CreateFixture(&viewportBoundsFixtureDef);
    
	// left
	viewportBoundsBox.Set(upperLeft, lowerLeft);
    viewportBoundsFixtureDef.shape = &viewportBoundsBox;
	_viewportBoundsBody->CreateFixture(&viewportBoundsFixtureDef);
    
	// right
	viewportBoundsBox.Set(lowerRight, upperRight);
    viewportBoundsFixtureDef.shape = &viewportBoundsBox;
	_viewportBoundsBody->CreateFixture(&viewportBoundsFixtureDef);
}

-(void) updateViewportRect:(CGSize)size withMargin:(float)marg
{
    NSMutableArray *fixturesToDestroy = [[[NSMutableArray alloc] init] autorelease];
    for (b2Fixture* f = _viewportBoundsBody->GetFixtureList(); f; f = f->GetNext())
    {
        //collect old fixtures for destroy
        [fixturesToDestroy addObject:[NSValue valueWithPointer:f]];
    }
    NSLog(@"Collected Fixtures to destroy from _viewportBoundsBody count %i",[fixturesToDestroy count]);
    for (NSValue *val in fixturesToDestroy)
    {
        _viewportBoundsBody->DestroyFixture((b2Fixture*)val.pointerValue);
    }
    
    float width = size.width;
	float height = size.height;
    
    //[ViewportController sharedViewportController].myInfo.rect = rect;

    GameActorInfo *boundsInfo = (GameActorInfo*)_viewportBoundsBody->GetUserData();
    CGPoint pos = boundsInfo.position;
    pos = [[ViewportController sharedViewportController] centerOfViewportOnLayer];
    float x = pos.x;
    float y = pos.y;
    
	float32 margin = marg;
	b2Vec2 lowerLeft = b2Vec2((x-width/2+margin)/PTM_RATIO, (y-height/2 + margin)/PTM_RATIO);
	b2Vec2 lowerRight = b2Vec2((x+(width/2-margin))/PTM_RATIO,(y-height/2 +margin)/PTM_RATIO);
	b2Vec2 upperRight = b2Vec2((x+(width/2-margin))/PTM_RATIO, (y+(height/2-margin))/PTM_RATIO);
	b2Vec2 upperLeft = b2Vec2((x-width/2+margin)/PTM_RATIO, (y+(height/2-margin))/PTM_RATIO);
    
    // Define the ground box shape.
	b2EdgeShape viewportBoundsBox;		
    
	// bottom
	viewportBoundsBox.Set(lowerLeft, lowerRight);
	_viewportBoundsBody->CreateFixture(&viewportBoundsBox,0);
    
	// top
	viewportBoundsBox.Set(upperRight, upperLeft);
	_viewportBoundsBody->CreateFixture(&viewportBoundsBox,0);
    
	// left
	viewportBoundsBox.Set(upperLeft, lowerLeft);
	_viewportBoundsBody->CreateFixture(&viewportBoundsBox,0);
    
	// right
	viewportBoundsBox.Set(lowerRight, upperRight);
	_viewportBoundsBody->CreateFixture(&viewportBoundsBox,0);
}

#pragma mark -
#pragma mark Tick

-(void)tick:(ccTime)dt
{
    
    
    
    
    // apply impulse
    
    for(b2Body *b = _world->GetBodyList(); b; b=b->GetNext()) {
        if (b->GetUserData() != NULL) {
            GameActorInfo *info = (GameActorInfo*)b->GetUserData();
            // search forced positions first
            bool forcedReposition = NO;
            if (info.attType == attPlayer)
            {
                PlayerActorInfo *playerInfo = (PlayerActorInfo*)info;
                if (playerInfo.forcedPosition.x != -1 || playerInfo.forcedPosition.y != -1)
                {
                    b2Vec2 b2Position = b2Vec2(playerInfo.forcedPosition.x/PTM_RATIO,
                                               playerInfo.forcedPosition.y/PTM_RATIO);
                    float32 b2Angle = -1 * CC_DEGREES_TO_RADIANS(playerInfo.rotation);
                    b->SetTransform(b2Position, b2Angle);
                    
                    playerInfo.forcedPosition = ccp(-1,-1);
                    
                    forcedReposition = YES;
                }
            }
            if ((!forcedReposition) && (info.b2bvelocity.x!=0 || info.b2bvelocity.y != 0))
            {
                b2Vec2 vel = b2Vec2(info.b2bvelocity.x/PTM_RATIO, info.b2bvelocity.y/PTM_RATIO);
                
                b->SetLinearVelocity(vel);
                //b->ApplyLinearImpulse(imp, b->GetWorldCenter());
                
                info.b2bvelocity = CGPointZero;
            }
            
            if (info.attType == attProjectile)
            {
                ProjectileActorInfo *projInfo = (ProjectileActorInfo*)info;
                if (!projInfo.impulseApplied && (projInfo.b2impulse.x!=0 || projInfo.b2impulse.y!=0))
                {
                    b2Vec2 imp = b2Vec2(info.b2impulse.x/PTM_RATIO, info.b2impulse.y/PTM_RATIO);
                    b->ApplyLinearImpulse(imp, b->GetWorldCenter());
                    
                    projInfo.b2impulse = CGPointZero;
                    projInfo.impulseApplied = YES;
                }
            }
            
            // reposition viewport bounds
            if (info.attType == attViewportBounds)
            {
                b2Vec2 b2Position = b2Vec2(info.position.x/PTM_RATIO,
                                           info.position.y/PTM_RATIO);
                float32 b2Angle = -1 * CC_DEGREES_TO_RADIANS(info.rotation);
                
                b->SetTransform(b2Position, b2Angle);
            }
        }
    }
    // do step
    _world->Step(dt, 6, 3);
    
    // update positions for cocos2d via their gameActorInfo
    for(b2Body *b = _world->GetBodyList(); b; b=b->GetNext()) {
        if (b->GetUserData() != NULL) {
            GameActorInfo *info = (GameActorInfo*)b->GetUserData();
                                 
            info.position = ccp(b->GetPosition().x * PTM_RATIO,
                                  b->GetPosition().y * PTM_RATIO);
            info.rotation = -1 * CC_RADIANS_TO_DEGREES(b->GetAngle());
        }
    }
    
    // lookup for contacts in contact listener, attached to the world
    std::vector<MyContact>::iterator pos;
    for(pos = _contactListener->_contacts.begin(); 
        pos != _contactListener->_contacts.end(); ++pos) {
        MyContact contact = *pos;
        
        b2Body *bodyA = contact.fixtureA->GetBody();
        b2Body *bodyB = contact.fixtureB->GetBody();
        if (bodyA->GetUserData() != NULL && bodyB->GetUserData() != NULL) {
            GameActorInfo *infoA = (GameActorInfo*) bodyA->GetUserData();
            GameActorInfo *infoB = (GameActorInfo*) bodyB->GetUserData();
            
            [self manageContactWithActorInfo:infoA andActorInfo:infoB];
        }        
    }
    
    // callbacks
    
    // after managing contacts we report possible safe points for players
    [self reportPossibleSafePointsForPlayers];
}

-(void)drawDebug
{
    return;
	//
	// IMPORTANT:
	// This is only for debug purposes
	// It is recommend to disable it
	//
	
	ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position );
	
	kmGLPushMatrix();
	
	_world->DrawDebugData();	
	
	kmGLPopMatrix();
}

#pragma mark -
#pragma mark - Add box2d body methods

-(void)addBoxBodyWithCircleShapeForGameObjectWithInfo:(GameActorInfo *)info
{
    b2BodyDef spriteBodyDef;
    SHBodyInitType initParams = [self switchInitTypeWithGOT:info.gotType andATT:info.attType];
    spriteBodyDef.type = initParams.btype;
    spriteBodyDef.linearDamping = initParams.linearDamping;
    spriteBodyDef.bullet = initParams.isBullet;
    if (initParams.isPlayer)
        [_players addObject:info];

    spriteBodyDef.position.Set(info.position.x/PTM_RATIO, 
                               info.position.y/PTM_RATIO);
    spriteBodyDef.userData = info;
    b2Body *spriteBody = _world->CreateBody(&spriteBodyDef);
    
    // output the body for info into the wrapper
    info.b2dBodyHolder.theB2Body = spriteBody;
    info.b2dBodyHolder.theWorld = _world;
    
    b2CircleShape spriteShape;
    float circleRadius;
    if (info.rect.size.width>info.rect.size.height)
        circleRadius = info.rect.size.width/2/PTM_RATIO;
    else
        circleRadius = info.rect.size.height/2/PTM_RATIO;
    spriteShape.m_radius = circleRadius;
    
    b2FixtureDef spriteShapeDef;
    spriteShapeDef.shape = &spriteShape;
    spriteShapeDef.density = 10.0f;
    spriteShapeDef.restitution = initParams.restitution;
    spriteShapeDef.isSensor = initParams.isSensor;
    spriteShapeDef.filter.categoryBits = initParams.categoryBits;
    spriteShapeDef.filter.maskBits = initParams.maskBits;
    
    spriteBody->CreateFixture(&spriteShapeDef);
    
}

-(void)addBoxBodyWithPolygonShapeForGameObjectWithInfo:(GameActorInfo *)info
{
    b2BodyDef spriteBodyDef;
    SHBodyInitType initParams = [self switchInitTypeWithGOT:info.gotType andATT:info.attType];
    spriteBodyDef.type = initParams.btype;
    spriteBodyDef.linearDamping = initParams.linearDamping;
    spriteBodyDef.bullet = initParams.isBullet;
    if (initParams.isPlayer)
        [_players addObject:info];
    
    if (initParams.isBase)
    {
        
    }

    
    spriteBodyDef.position.Set(info.position.x/PTM_RATIO, 
                               info.position.y/PTM_RATIO);
    spriteBodyDef.userData = info;
    b2Body *spriteBody = _world->CreateBody(&spriteBodyDef);
    
    // output the body for info into the wrapper
    info.b2dBodyHolder.theB2Body = spriteBody;
    info.b2dBodyHolder.theWorld = _world;
    
    b2PolygonShape spriteShape;
    spriteShape.SetAsBox(info.rect.size.width/PTM_RATIO/2,
                         info.rect.size.height/PTM_RATIO/2);
    
    b2FixtureDef spriteShapeDef;
    spriteShapeDef.shape = &spriteShape;
    spriteShapeDef.density = 10.0;
    spriteShapeDef.restitution = initParams.restitution;
    spriteShapeDef.isSensor = initParams.isSensor;
    spriteShapeDef.filter.categoryBits = initParams.categoryBits;
    spriteShapeDef.filter.maskBits = initParams.maskBits;
    
    spriteBody->CreateFixture(&spriteShapeDef);
}

-(void)addBoxBodyWithPolygonShapeForGameObjectWithInfo:(GameActorInfo *)info with:(SHTriangleShapeType)tstType
{
    b2BodyDef spriteBodyDef;
    SHBodyInitType initParams = [self switchInitTypeWithGOT:info.gotType andATT:info.attType];
    spriteBodyDef.type = initParams.btype;
    spriteBodyDef.linearDamping = initParams.linearDamping;
    spriteBodyDef.bullet = initParams.isBullet;
    if (initParams.isPlayer)
        [_players addObject:info];
    
    
    spriteBodyDef.position.Set(info.position.x/PTM_RATIO, 
                               info.position.y/PTM_RATIO);
    spriteBodyDef.userData = info;
    b2Body *spriteBody = _world->CreateBody(&spriteBodyDef);
    
    // output the body for info into the wrapper
    info.b2dBodyHolder.theB2Body = spriteBody;
    info.b2dBodyHolder.theWorld = _world;
    
    float width = info.rect.size.width;
	float height = info.rect.size.height;
    
	float32 margin = 0.0f;
	b2Vec2 lowerLeft = b2Vec2(margin-width/2/PTM_RATIO, margin-height/2/PTM_RATIO);
	b2Vec2 lowerRight = b2Vec2((width/2-margin)/PTM_RATIO,margin-height/2/PTM_RATIO);
	b2Vec2 upperRight = b2Vec2((width/2-margin)/PTM_RATIO, (height/2-margin)/PTM_RATIO);
	b2Vec2 upperLeft = b2Vec2(margin-width/2/PTM_RATIO, (height/2-margin)/PTM_RATIO);
    
    b2Vec2 vertices[3];
    int32 count = 3;
    switch (tstType) {
        case tstLDC:
            vertices[0].Set(upperLeft.x, upperLeft.y);
            vertices[1].Set(lowerLeft.x, lowerLeft.y);
            vertices[2].Set(lowerRight.x, lowerRight.y);
            break;
            
        case tstLUC:
            vertices[0].Set(upperRight.x, upperRight.y);
            vertices[1].Set(upperLeft.x, upperLeft.y);
            vertices[2].Set(lowerLeft.x, lowerLeft.y);
            break;
            
        case tstRUC:
            vertices[0].Set(upperRight.x, upperRight.y);
            vertices[1].Set(upperLeft.x, upperLeft.y);
            vertices[2].Set(lowerRight.x, lowerRight.y);
            break;
            
        case tstRDC:
            vertices[0].Set(lowerRight.x, lowerRight.y);
            vertices[1].Set(upperRight.x, upperRight.y);
            vertices[2].Set(lowerLeft.x, lowerLeft.y);
            break;
            
        default:
            CCLOG(@"### WARNING! SHTriangleShapeType undefined while creating box2d body");
            break;
    }
    
    b2PolygonShape spriteShape;
    spriteShape.Set(vertices, count);
    
    b2FixtureDef spriteShapeDef;
    spriteShapeDef.shape = &spriteShape;
    spriteShapeDef.density = 10.0;
    spriteShapeDef.restitution = initParams.restitution;
    spriteShapeDef.isSensor = initParams.isSensor;
    spriteShapeDef.filter.categoryBits = initParams.categoryBits;
    spriteShapeDef.filter.maskBits = initParams.maskBits;
    
    spriteBody->CreateFixture(&spriteShapeDef);
}

-(struct SHBodyInitType)switchInitTypeWithGOT:(gameObjectType)got andATT:(attachedToType)att
{
    SHBodyInitType initType;
    switch (got) {
        case gotStatic:
            switch (att) {
                case attCollidableTile:
                    initType.btype = b2_staticBody;
                    initType.isSensor = false;
                    initType.isPlayer = false;
                    initType.isBase = false;
                    initType.linearDamping = 0;
                    initType.isBullet = false;
                    initType.restitution = 0;
                    initType.categoryBits = CATEGORY_BOUNDRY;
                    initType.maskBits = CATEGORY_PROJECTILE | CATEGORY_PLAYER | CATEGORY_MONSTER;
                    break;
                    
                case attBaseFirst:
                case attBaseSecond:
                    initType.btype = b2_staticBody;
                    initType.isSensor = false;
                    initType.isPlayer = false;
                    initType.isBase = true;
                    initType.linearDamping = 0;
                    initType.isBullet = false;
                    initType.restitution = 0;
                    initType.categoryBits = CATEGORY_BOUNDRY;
                    initType.maskBits = CATEGORY_PROJECTILE | CATEGORY_PLAYER | CATEGORY_MONSTER;
                    break;
                    
                default:
                    CCLOGERROR(@"### WARNING! switchInitType got unrecognized attachedToType %i.", att);
                    break;
            }
            
            break;
            
        case gotDynamic:
            switch (att) {
                case attPlayer:
                    initType.btype = b2_dynamicBody;
                    initType.isSensor = false;
                    initType.isPlayer = true;
                    initType.isBase = false;
                    initType.linearDamping = 6.0f;
                    initType.isBullet = false;
                    initType.restitution = 0;
                    initType.categoryBits = CATEGORY_PLAYER;
                    initType.maskBits = CATEGORY_BOUNDRY | CATEGORY_MONSTER | CATEGORY_PROJECTILE | CATEGORY_PLAYER;
                    break;
                    
                case attProjectile:
                    initType.btype = b2_dynamicBody;
                    initType.isSensor = false;
                    initType.isPlayer = false;
                    initType.isBase = false;
                    initType.linearDamping = 0;
                    initType.isBullet = true;
                    initType.restitution = 1.0f;
                    initType.categoryBits = CATEGORY_PROJECTILE;
                    initType.maskBits = CATEGORY_BOUNDRY | CATEGORY_MONSTER | CATEGORY_PLAYER;
                    break;
                    
                case attMonster:                    
                    initType.btype = b2_dynamicBody;
                    initType.isSensor = false;
                    initType.isPlayer = false;
                    initType.isBase = false;
                    initType.linearDamping = 4.0f;
                    initType.isBullet = false;
                    initType.restitution = 0;
                    initType.categoryBits = CATEGORY_MONSTER;
                    initType.maskBits = CATEGORY_BOUNDRY | CATEGORY_MONSTER | CATEGORY_PLAYER | CATEGORY_PROJECTILE;
                    break;
                    
                default:
                    CCLOGERROR(@"### WARNING! switchInitType got unrecognized attachedToType %i.", att);
                    break;
            }
            
            break;
            
            
        default:
            CCLOGERROR(@"### WARNING! switchInitType got unrecognized gameObjectType %i.",got);
            break;
    }
    return initType;
}

#pragma mark -
#pragma mark Manage Contact

-(void)manageContactWithActorInfo:(GameActorInfo *)infoA andActorInfo:(GameActorInfo *)infoB
{
    if (infoA.gotType == gotDynamic && infoB.gotType == gotStatic) //
    {
        [self manageContactWithDynamicActorInfo:infoA andStaticActorInfo:infoB];
    }
    else if (infoA.gotType == gotStatic && infoB.gotType == gotDynamic)
    {
        [self manageContactWithDynamicActorInfo:infoB andStaticActorInfo:infoA];
    }
    else if (infoA.gotType == gotDynamic && infoB.gotType == gotDynamic)
    {
        [self manageContactWithDynamicActorInfo:infoA andDynamicActorInfo:infoB];
    }
    else if (infoA.gotType == gotStatic && infoB.gotType == gotStatic)
    {
        // it can only be boundsBody colliding with others...
    }
}

////////// DYNAMIC VS STATIC
-(void)manageContactWithDynamicActorInfo:(GameActorInfo *)dynInfo 
                      andStaticActorInfo:(GameActorInfo *)staticInfo
{
    
    if (dynInfo.attType == attPlayer)
    {
        // player vs collidable tile collision
        if (staticInfo.attType == attCollidableTile)
        {
            PlayerActorInfo *playerInfo = (PlayerActorInfo*)dynInfo;
            playerInfo.b2dContactDetected = YES;
            [playerInfo pushContact:staticInfo];
        }
    }
    
    else if (dynInfo.attType == attProjectile)
    {
        
        ProjectileActorInfo *projInfo = (ProjectileActorInfo*)dynInfo;
        // projectile vs collidable tile collision
        if (staticInfo.attType == attCollidableTile)
        {
            projInfo.b2dContactDetected = YES;
            projInfo.collidedObject = staticInfo;
        }
        // projectile vs map bounds collision
        else if (staticInfo.attType == attMapBounds)
        {
            projInfo.b2dContactDetected = YES;
            projInfo.collidedObject = staticInfo;
        }
        // projectile vs base collision
        else if (staticInfo.attType == attBaseFirst || staticInfo.attType == attBaseSecond)
        {
            projInfo.b2dContactDetected = YES;
            projInfo.collidedObject = staticInfo;
            BaseActorInfo *baseInfo = (BaseActorInfo*)staticInfo;
            [baseInfo pushContact:projInfo];
        }
    }
    

}

////////// DYNAMIC VS DYNAMIC
-(void)manageContactWithDynamicActorInfo:(GameActorInfo *)dynInfoA 
                     andDynamicActorInfo:(GameActorInfo *)dynInfoB
{
    // player vs projectile
    if ((dynInfoA.attType == attPlayer && dynInfoB.attType == attProjectile)
        || (dynInfoA.attType == attProjectile && dynInfoB.attType == attPlayer))
    {
        PlayerActorInfo *playerInfo = nil;
        ProjectileActorInfo *projectileInfo = nil;
        if (dynInfoA.attType == attPlayer) {
            playerInfo = (PlayerActorInfo*)dynInfoA;
            projectileInfo = (ProjectileActorInfo*)dynInfoB;
        }
        else {
            playerInfo = (PlayerActorInfo*)dynInfoB;
            projectileInfo = (ProjectileActorInfo*)dynInfoA;
        }
        
        if (![projectileInfo actorAlreadyContacted:playerInfo])
        {
            projectileInfo.collidedObject = playerInfo;
            playerInfo.b2dContactDetected = projectileInfo.b2dContactDetected = YES;
            [playerInfo pushContact:projectileInfo];
            [projectileInfo pushContact:playerInfo];
        }
    }
    // monster vs projectile
    if ((dynInfoA.attType == attProjectile && dynInfoB.attType == attMonster) ||
        (dynInfoA.attType == attMonster && dynInfoB.attType == attProjectile))
    {
        MonsterActorInfo *monsterInfo = nil;
        ProjectileActorInfo *projInfo = nil;
        if (dynInfoA.attType == attMonster) {
            monsterInfo = (MonsterActorInfo*)dynInfoA;
            projInfo = (ProjectileActorInfo*)dynInfoB;
        }
        else {
            monsterInfo = (MonsterActorInfo*)dynInfoB;
            projInfo = (ProjectileActorInfo*)dynInfoA;
        }
        if (![projInfo actorAlreadyContacted:monsterInfo])
        {
            projInfo.collidedObject = monsterInfo;
            monsterInfo.b2dContactDetected = projInfo.b2dContactDetected = YES;
            [monsterInfo pushContact:projInfo];
            [projInfo pushContact:monsterInfo];
        }
    }
        
}


#pragma mark -
#pragma mark Shooter specific callbacks

-(void)reportPossibleSafePointsForPlayers;
{
    for (PlayerActorInfo *pinfo in _players)
    {
        if (pinfo.b2dContactDetected)
        {
            // no safe point 
            pinfo.b2dContactDetected = NO;
            pinfo.collisionDetected = YES;
            pinfo.collisionWorkedOutAndSolved = NO;
        }
        else
        {
            // erase previous collision info
            pinfo.collisionDetected = NO;
            pinfo.safePoint = pinfo.position;
        }
    }
}

#pragma mark -
#pragma mark Non box2d collision tests

-(bool)rect:(CGRect)rect intersectsCircleWithOrigin:(CGPoint)p1 andRadius:(CGFloat)r
{
    CGPoint circleDistance;
    circleDistance.x = fabsf(p1.x - rect.origin.x - rect.size.width/2);
    circleDistance.y = fabsf(p1.y - rect.origin.y - rect.size.height/2);
    
    if (circleDistance.x > (rect.size.width/2 + r)) { return false; }
    if (circleDistance.y > (rect.size.height/2 + r)) { return false; }
    
    if (circleDistance.x <= (rect.size.width/2)) { return true; } 
    if (circleDistance.y <= (rect.size.height/2)) { return true; }
    
    float cornerDistance_sq = powf((circleDistance.x - rect.size.width/2),2) +
    powf((circleDistance.y - rect.size.height/2),2);
    
    return (cornerDistance_sq <= powf(r, 2));
}



-(CGRect)calculateRectForPosition:(CGPoint)position andSize:(CGSize)size
{
    return CGRectMake(position.x-size.width/2,
                      position.y-size.height/2,
                      size.width,
                      size.height);
}

-(bool)rectCollisionTestForActorWithInfo:(GameActorInfo *)info1 andActorWithInfo:(GameActorInfo *)info2
{
    return CGRectIntersectsRect(info1.rect, info2.rect);
}
//-(void)addStaticBoxBodyWithPolygonShapeForRect:(CGRect)rect
//{
//    b2BodyDef spriteBodyDef;
//    spriteBodyDef.type = b2_staticBody;
//    spriteBodyDef.position.Set((rect.origin.x+rect.size.width/2)/PTM_RATIO, 
//                               (rect.origin.y+rect.size.height/2)/PTM_RATIO);
//    //spriteBodyDef.userData = nil;
//    b2Body *spriteBody = _world->CreateBody(&spriteBodyDef);
//    
//    b2PolygonShape spriteShape;
//    spriteShape.SetAsBox(rect.size.width/PTM_RATIO/2,
//                         rect.size.height/PTM_RATIO/2);
//    
//    b2FixtureDef spriteShapeDef;
//    spriteShapeDef.shape = &spriteShape;
//    spriteShapeDef.density = 10.0;
//    spriteShapeDef.isSensor = true;
//    spriteBody->CreateFixture(&spriteShapeDef);
//}
@end
