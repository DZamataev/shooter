//
//  SHPlayerWeapon.h
//  Anti-heroes Wars
//
//  Created by Denis on 4/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Projectile.h"


struct SHPlayerWeaponCharacteristics 
{
    projectileType typeOfProjectile;
    int damage;
    int bounces;
    int penetration;
    int ammoInCase;
    float firingRate;
    float speed;
    float collisionRadius;
};

@interface SHPlayerWeapon : NSObject
{
    NSString *_modelName;
    struct SHPlayerWeaponCharacteristics _characteristics;
}
@property (nonatomic, retain) NSString *modelName;
@property struct SHPlayerWeaponCharacteristics characteristics;
-(id)initWithModelName:(NSString*)model;
-(id)initWithCharacteristics:(struct SHPlayerWeaponCharacteristics)characteristics;
@end
