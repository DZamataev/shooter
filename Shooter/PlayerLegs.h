//
//  PlayerLegs.h
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PlayerBody.h"
#import "PlayerActorInfo.h"
#import "SHPlayerMovementDelegate.h"

@interface PlayerLegs : CCNode {
    
    CCAnimate* _stillStep;
    CCAnimate* _leftStep;
    CCAnimate* _rightStep;
    bool _isMoving;
    CGPoint _previousPos;
    id <SHPlayerMovementDelegate> _movementDelegate;
    
    CCSprite *sprite;
    PlayerBody *myBody;
    CGPoint _normal;
    CGPoint _joyPosition;
    CGPoint _speedControlFromJoy;
    PlayerActorInfo *_myInfo;
}
@property CGPoint normal;
@property (nonatomic,retain) PlayerActorInfo *myInfo;
@property (nonatomic, retain) CCAnimate *stillStep;
@property (nonatomic, retain) CCAnimate *leftStep;
@property (nonatomic, retain) CCAnimate *rightStep;
@property (nonatomic, assign) id <SHPlayerMovementDelegate> movementDelegate;
@property bool isMoving;

-(id)initWithBody:(PlayerBody*)body;
-(void)receiveControlFromJoystick:(CGPoint)point;
-(void)repositionLegs:(CGPoint)position;
-(void)update:(ccTime)dt;
-(PlayerActorInfo*)setupPhysicsBody;

-(void)runStepActionOnSprite:(CCAction*)action;
-(void)stopAllActionsOnSprite;
@end
