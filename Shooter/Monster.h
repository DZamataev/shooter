//
//  Monster.h
//  Anti-heroes Wars
//
//  Created by Denis on 4/8/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class MonsterActorInfo;

typedef enum {MCZombie}MonsterClass;

typedef enum {MSStill = 0, MSWalk, MSAttack, MSDie}MonsterState;

@interface Monster : CCNode {
    CCSprite *sprite;
    MonsterState _state;
    MonsterClass _monsterClass;
    CCAnimate *_actStill;
    CCAnimate *_actWalk;
    CCAnimate *_actAttack;
    CCAnimate *_actDie;
    
    float _feelRange;
    
    MonsterActorInfo *_myInfo;
}

@property MonsterState state;
@property MonsterClass monsterClass;
@property (nonatomic, retain) CCAnimate *actStill;
@property (nonatomic, retain) CCAnimate *actWalk;
@property (nonatomic, retain) CCAnimate *actAttack;
@property (nonatomic, retain) CCAnimate *actDie;
@property float feelRange;
@property (nonatomic, retain) MonsterActorInfo *myInfo;
@end
