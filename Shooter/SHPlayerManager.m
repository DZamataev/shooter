//
//  SHPlayerManager.m
//  Anti-heroes Wars
//
//  Created by Denis on 4/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SHPlayerManager.h"
#import "SHPlayerLoader.h"
#import "SHPlayerWeapon.h"
#import "Projectile.h"

@implementation SHPlayerManager
CWL_SYNTHESIZE_SINGLETON_FOR_CLASS(SHPlayerManager)
-(SHPlayerLoader*)getFirstPlayerLoader
{
    struct SHPlayerWeaponCharacteristics weaponChars;
    weaponChars.damage = 1;
    weaponChars.firingRate = 0.1;
    weaponChars.bounces = 0;
    weaponChars.penetration = 0;
    weaponChars.ammoInCase = 20;
    weaponChars.speed = 80.0f;
    weaponChars.collisionRadius = 10.0f;
    weaponChars.typeOfProjectile = projectileFireBall;
    SHPlayerLoader *result = [[SHPlayerLoader alloc] initWithModelName:@"g22" andColorProperty:@"blue" andStartWeapon:[[SHPlayerWeapon alloc] initWithCharacteristics:weaponChars]];
    return result;
}

-(SHPlayerLoader*)getSecondPlayerLoader
{
    struct SHPlayerWeaponCharacteristics weaponChars;
    weaponChars.damage = 2;
    weaponChars.firingRate = 0.15;
    weaponChars.bounces = 0;
    weaponChars.penetration = 1;
    weaponChars.ammoInCase = 20;
    weaponChars.speed = 80.0f;
    weaponChars.collisionRadius = 10.0f;
    weaponChars.typeOfProjectile = projectileFireBall;
    SHPlayerLoader *result = [[SHPlayerLoader alloc] initWithModelName:@"g22" andColorProperty:@"blue" andStartWeapon:[[SHPlayerWeapon alloc] initWithCharacteristics:weaponChars]];
    return result;
}
@end
