//
//  MenuLayer.m
//  SomeShooter
//
//  Created by Denis on 3/17/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "MenuLayer.h"
#import "SimpleAudioEngine.h"


@implementation MenuLayer
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	MenuLayer *layer = [MenuLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
    // preload music

    
	// return the scene
	return scene;
}

-(id)init
{
    self = [super init];
    CGSize winSize = [CCDirector sharedDirector].winSize;
    CCLabelTTF *gameplayLabel = [CCLabelTTF 
                                 labelWithString:@"Gameplay" 
                                 fontName:@"Marker Felt" fontSize:50];
    CCLabelTTF *testLabel = [CCLabelTTF 
                             labelWithString:@"Test room" 
                             fontName:@"Marker Felt" fontSize:50];
    CCMenuItemLabel *gameplayBut = [CCMenuItemLabel itemWithLabel:gameplayLabel target:self selector:@selector(switchToGameplay)];
    CCMenuItemLabel *testBut = [CCMenuItemLabel itemWithLabel:testLabel target:self selector:@selector(switchToTest)];

    CCLabelTTF *musicLabel = [CCLabelTTF labelWithString:@"Enable music" fontName:@"Marker felt" fontSize:50];
    CCMenuItemLabel *musicBut = [CCMenuItemLabel itemWithLabel:musicLabel target:self selector:@selector(enableMusic)];
    
    CCMenu *musicMenu = [CCMenu menuWithItems:musicBut, nil];
    musicMenu.position = ccp(winSize.width*0.8, winSize.height*0.1);
    [self addChild:musicMenu];
    
    CCMenu *mainMenu = [CCMenu menuWithItems:gameplayBut, testBut, nil];
    
    [mainMenu alignItemsVerticallyWithPadding:40.0f];
    mainMenu.scale = 1.0f;
    [self addChild:mainMenu];
    return self;
}

-(void)onEnter
{
    [super onEnter];
}

-(void)switchToGameplay
{

    [[CCDirector sharedDirector] replaceScene:[CCTransitionMoveInR transitionWithDuration:1.0f scene:[GameplayLayer scene]]];
}

-(void)switchToTest
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionMoveInR transitionWithDuration:1.0f scene:[TestLayer scene]]];
}

-(void)enableMusic
{
    [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"Music/Syndicate.mp3"];
    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"Music/Syndicate.mp3"];
}
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
@end
