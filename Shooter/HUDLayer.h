//
//  HUDLayer.h
//  Shooter
//
//  Created by Denis on 3/17/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class GameplayLayer;
@class SHPlayerHealth;

@protocol HUDLayerDelegate <NSObject>

@end

@interface HUDLayer : CCLayer <HUDLayerDelegate> {
    GameplayLayer *_gameplayLayer;
    
    // health display
    SHPlayerHealth *_firstPlayerHealth;
    SHPlayerHealth *_secondPlayerHealth;
    CCLabelTTF *_firstPlayerHealthLabel;
    CCLabelTTF *_secondPlayerHealthLabel;
    //
}
@property (nonatomic, assign) GameplayLayer *gameplayLayer;
@property (nonatomic, assign) SHPlayerHealth *firstPlayerHealth;
@property (nonatomic, assign) SHPlayerHealth *secondPlayerHealth;

-(void)callSlowMo;
-(void)callZoomIn;
-(void)callZoomOut;

@end
