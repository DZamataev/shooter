//
//  PlayerController.m
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "PlayerController.h"


@implementation PlayerController

-(id)initWithPlayer:(Player *)player atCenterPoint:(CGPoint)centerPoint withMargin:(float)margin isVertical:(_Bool)vertical isReversed:(_Bool)reversed
{
    self = [super init];
    int reverse = 0;
    if (reversed)
        reverse = -1;
    else
        reverse = 1;
    //Joystick 1 for player 1
    joy1	= [ZJoystick joystickNormalSpriteFile:@"Interface/Joystick/JoystickContainer_norm.png" selectedSpriteFile:@"Interface/Joystick/JoystickContainer_trans.png" controllerSpriteFile:@"Interface/Joystick/Joystick_norm.png"];
    PlayerControllerJoystickDispatcher *dispatcher1 = [[PlayerControllerJoystickDispatcher alloc] initWithTag:1 andDelegate:self];
    joy1.delegate	= dispatcher1;				//Joystick Delegate
    joy1.controlledObject  = player.legs;     //we set our controlled object which the blue circle
    joy1.speedRatio         = 300.0f;                //we set speed ratio, movement speed of blue circle once controlled to any direction
    joy1.joystickRadius     = 50.0f;               //Added in v1.2
    joy1.borderCutEnabled   = NO;                   // Shooter specific option
    
    
    
    //Joystick 2 for player 1
    joy2	= [ZJoystick joystickNormalSpriteFile:@"Interface/Joystick/JoystickContainer_norm.png" selectedSpriteFile:@"Interface/Joystick/JoystickContainer_trans.png" controllerSpriteFile:@"Interface/Joystick/Joystick_norm.png"];
    joy2.position	= ccp(joy2.contentSize.width/2, joy2.contentSize.height/2);
    PlayerControllerJoystickDispatcher *dispatcher2 = [[PlayerControllerJoystickDispatcher alloc] initWithTag:2 andDelegate:self];
    joy2.delegate	= dispatcher2;				//Joystick Delegate
    joy2.controlledObject  = player.body;     //we set our controlled object which the blue circle
    joy2.speedRatio         = 3.5f;                //we set speed ratio, movement speed of blue circle once controlled to any direction
    joy2.joystickRadius     = 50.0f;               //Added in v1.2
    joy2.borderCutEnabled   = NO;                   // Shooter specific option
    
    
    if (vertical)
    {
        joy1.position = ccp(centerPoint.x, ((reverse * margin) + centerPoint.y));
        joy2.position = ccp(centerPoint.x, ((reverse * margin * -1) + centerPoint.y));
    }
    else
    {
        joy1.position = ccp(((reverse * margin) + centerPoint.x),centerPoint.y);
        joy2.position = ccp(((reverse * margin * -1) + centerPoint.x), centerPoint.y);
    }
    
    
    [self addChild:joy1];
    [self addChild:joy2];
    
    _player = player;
    
    return self;
}

-(void)joystickControlBeganWithTag:(int)joyTag
{
    if (joyTag == 2)
    {
        _player.body.bodyJoystickIsTouched = YES;
    }
}

-(void)joystickControlDidUpdate:(id)joystick toXSpeedRatio:(CGFloat)xSpeedRatio toYSpeedRatio:(CGFloat)ySpeedRatio withTag:(int)joyTag
{
    if (joyTag == 1) // send control to legs
    {
        [_player.legs receiveControlFromJoystick:ccp(xSpeedRatio,ySpeedRatio)];
    }
}

-(void)joystickControlEndedWithTag:(int)joyTag
{
    if (joyTag == 2)
    {
        _player.body.bodyJoystickIsTouched = NO;
    }
}

-(void)joystickControlMovedWithTag:(int)joyTag
{
    
}


@end
