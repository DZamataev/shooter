//
//  MyContactListener.m
//  Box2DPong
//
//  Created by Ray Wenderlich on 2/18/10.
//  Copyright 2010 Ray Wenderlich. All rights reserved.
//

#import "MyContactListener.h"
#import "GameActorInfo.h"
#import "ProjectileActorInfo.h"

MyContactListener::MyContactListener() : _contacts() {
}

MyContactListener::~MyContactListener() {
}

void MyContactListener::BeginContact(b2Contact* contact) {
    // We need to copy out the data because the b2Contact passed in
    // is reused.
    MyContact myContact = { contact->GetFixtureA(), contact->GetFixtureB() };
    _contacts.push_back(myContact);
    
}

void MyContactListener::EndContact(b2Contact* contact) {
    MyContact myContact = { contact->GetFixtureA(), contact->GetFixtureB() };
    std::vector<MyContact>::iterator pos;
    pos = std::find(_contacts.begin(), _contacts.end(), myContact);
    if (pos != _contacts.end()) {
        _contacts.erase(pos);
    }
}

void MyContactListener::PreSolve(b2Contact* contact, const b2Manifold* oldManifold) {
    GameActorInfo* actorInfo1 = (GameActorInfo*)contact->GetFixtureA()->GetBody()->GetUserData();
    GameActorInfo* actorInfo2 = (GameActorInfo*)contact->GetFixtureB()->GetBody()->GetUserData();
    // filter contact for penetration if projectile
    if (actorInfo1.attType == attProjectile || actorInfo2.attType == attProjectile)
    {
        ProjectileActorInfo *projInfo;
        GameActorInfo *possiblyPenetratedInfo;
        if (actorInfo1.attType == attProjectile)
        {
            projInfo = (ProjectileActorInfo*)actorInfo1;
            possiblyPenetratedInfo = (GameActorInfo*)actorInfo2;
        }
            
        else 
        {
            projInfo = (ProjectileActorInfo*)actorInfo2;
            possiblyPenetratedInfo = (GameActorInfo*)actorInfo1;
        }
        
        if (possiblyPenetratedInfo.attType == attPlayer || possiblyPenetratedInfo.attType == attMonster)
        {
        
            if (![projInfo actorAlreadyPenetrated:possiblyPenetratedInfo])
            {
                projInfo.penetration--;
                if (projInfo.penetration >= 0)
                {
                    // the penetration will be treated as bounce so we add one bounce for this penetration
                    projInfo.bounces++;
                    [projInfo pushPenetratedInfo:possiblyPenetratedInfo];
                    contact->SetEnabled(FALSE);
                }
            }
            else { 
                contact->SetEnabled(FALSE);
            }
        }
    }
}

void MyContactListener::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse) {
}

