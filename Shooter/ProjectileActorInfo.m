//
//  ProjectileActorInfo.m
//  Shooter
//
//  Created by Denis on 3/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProjectileActorInfo.h"

@implementation ProjectileActorInfo
@synthesize collidedObject = _collidedObject;
@synthesize damage = _damage;
@synthesize impulseApplied = _impulseApplied;
@synthesize bounces = _bounces;
@synthesize penetration = _penetration;
-(id)initWithPosition:(CGPoint)pos andRotation:(CGFloat)rotation andRect:(CGRect)rect andGotType:(gameObjectType)got andAttType:(attachedToType)att;
{
    self = [super initWithPosition:pos andRotation:rotation andRect:rect andGotType:got andAttType:att];
    _damage = 1;
    _collidedObject = nil;
    _impulseApplied = NO;
    _penetration = 0;
    _bounces = 0;
    _penetrated = [NSMutableArray new];
    _contacted = [NSMutableArray new];
    return self;
}

-(void)pushPenetratedInfo:(GameActorInfo *)info
{
    [_penetrated addObject:info];
}

-(bool)actorAlreadyPenetrated:(GameActorInfo *)info
{
    return [_penetrated containsObject:info];
}

-(void)pushContact:(GameActorInfo *)info
{
    [_contacted addObject:info];
}

-(bool)actorAlreadyContacted:(GameActorInfo *)info
{
    return [_contacted containsObject:info];
}

-(void)eraseContacted:(GameActorInfo *)info
{
    NSLog(@"count before:%i",[_contacted count]);
    [_contacted removeObject:info];
    NSLog(@"count after:%i",[_contacted count]);
}

-(void)dealloc
{
    [_contacted release];
    [_penetrated release];
    _collidedObject = nil;
    [super dealloc];
}
@end
