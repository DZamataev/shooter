//
//  SHMetaTile.m
//  Shooter
//
//  Created by Denis on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SHMetaTile.h"

@implementation SHMetaTile
@synthesize mapPosition = _mapPosition, gid = _gid;
-(id)initWithGid:(uint32_t)incGid andPosition:(CGPoint)incPos
{
    self = [super init];
    self.gid = incGid;
    self.mapPosition = incPos;
    return self;
}
@end
