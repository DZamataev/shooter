//
//  Player.h
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "PlayerBody.h"
#import "PlayerLegs.h"
#import "SHMonsterTargetProtocol.h"

@class SHPlayerLoader;
@class SHPlayerBehavior;
@class SHPlayerWeapon;



@interface Player : CCLayer <SHMonsterTargetProtocol> {
    PlayerBody *_body;
    PlayerLegs *_legs;
    CGPoint _startPoint;
    float _startAngle;
    PlayerActorInfo *_myInfo;
    
    // test loader
    SHPlayerLoader *_playerLoader;
    
    SHPlayerBehavior *_behavior;
    
    SHPlayerWeapon *_currentWeapon;
    
}
@property (nonatomic, retain) PlayerBody *body;
@property (nonatomic, retain) PlayerLegs *legs;
@property (nonatomic, assign) PlayerActorInfo *myInfo;
@property (nonatomic, retain) SHPlayerWeapon *currentWeapon;
@property CGPoint startPoint;
@property float startAngle;

-(id)initWithLoader:(SHPlayerLoader*)ploader;

-(void)setupPhysics;
-(void)teleportToPosition:(CGPoint)pos;

-(CCAnimate*)loadStillStepForLegs;
-(CCAnimate*)loadLeftStepForLegs;
-(CCAnimate*)loadRightStepForLegs;
-(CCAnimate*)loadStillBendForBody;
-(CCAnimate*)loadLeftBendForBody;
-(CCAnimate*)loadRightBendForBody;

// SHMonsterTargetProtocol methods...
@end
