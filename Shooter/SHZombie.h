//
//  SHZombie.h
//  Anti-heroes Wars
//
//  Created by Denis on 4/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Monster.h"

@class MonsterActorInfo;
@class SHZombieBehaviour;

@interface SHZombie : Monster
{
    NSString *_monsterClassString;
    int _damage;
    int _health;
    float _speedMod;
    SHZombieBehaviour *_behaviour;
    CGPoint _velocityToTarget;
    CGPoint _previousPosition;
    bool _isMoving;
    bool _isDestroyed;
    NSMutableArray *_everContactedProjectiles;
}
@property int damage;
@property int health;
@property (nonatomic, retain) SHZombieBehaviour *behaviour;
@property CGPoint velocityToTarget;
@property float speedMod;
@property bool isMoving;
@property bool isDestroyed;

-(id)initWithDefaultMonsterInfoAndPosition:(CGPoint)pos andBehaviour:(SHZombieBehaviour*)b;
-(void)changeState:(MonsterState)incState;
@end
