//
//  SHPlayerWeapon.m
//  Anti-heroes Wars
//
//  Created by Denis on 4/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SHPlayerWeapon.h"

@implementation SHPlayerWeapon
@synthesize modelName = _modelName;
@synthesize characteristics = _characteristics;
-(id)initWithModelName:(NSString *)model
{
    self = [super init];
    // here we should find the model name in some place where all weapons are
    return self;
}

-(id)initWithCharacteristics:(struct SHPlayerWeaponCharacteristics)incCharacteristics
{
    self = [super init];
    self.modelName = [NSString stringWithFormat:@"Undefined"];
    self.characteristics = incCharacteristics;
    return self;
}

-(void)dealloc
{
    self.modelName = nil;
    [super dealloc];
}
@end
