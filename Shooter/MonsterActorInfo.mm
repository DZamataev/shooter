//
//  MonsterActorInfo.mm
//  Anti-heroes Wars
//
//  Created by Denis on 4/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MonsterActorInfo.h"
#import "SHBox2dBodyHolder.h"

@implementation MonsterActorInfo
@synthesize forcedPosition = _forcedPosition;
@synthesize contactsInfo = _contactsInfo;
-(id)initWithPosition:(CGPoint)pos andRotation:(CGFloat)rotation andRect:(CGRect)rect andGotType:(gameObjectType)got andAttType:(attachedToType)att
{
    self = [super initWithPosition:pos andRotation:rotation andRect:rect andGotType:got andAttType:att];
    
    _forcedPosition = ccp(-1,-1);
    _contactsInfo = [NSMutableArray new];
    
    return self;
}

-(void)pushContact:(GameActorInfo*)contactInfo
{
    [_contactsInfo addObject:contactInfo];
}

-(void)removePhysicsBody
{
    [_b2dBodyHolder purgeStoredData];
    [_b2dBodyHolder release];
    _physicsBodyRemoved = YES;
}


-(void)dealloc
{
    [_contactsInfo release];
    [super dealloc];
}
@end
