//
//  SHZombie.mm
//  Anti-heroes Wars
//
//  Created by Denis on 4/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SHZombie.h"
#import "MonsterAnimationLoader.h"
#import "MonsterActorInfo.h"
#import "SHb2dCollisionManager.h"
#import "SHZombieBehaviour.h"

@implementation SHZombie
@synthesize damage = _damage;
@synthesize health = _health;
@synthesize velocityToTarget = _velocityToTarget;
@synthesize speedMod = _speedMod;
@synthesize isDestroyed = _isDestroyed;
-(id)init
{
    self = [super init];
    _velocityToTarget = ccp(0,0);
    _isMoving = _isDestroyed = NO;
    _damage = 1;
    _health = 5;
    _speedMod = 100.0f;
    _state = MSStill;
    _monsterClass = MCZombie;
    _monsterClassString = [NSString stringWithFormat:@"zombie"];
    self.actAttack = [MonsterAnimationLoader loadAnimateWithClassName:_monsterClassString andState:MSAttack];
    self.actStill = [MonsterAnimationLoader loadAnimateWithClassName:_monsterClassString andState:MSStill];
    self.actWalk = [MonsterAnimationLoader loadAnimateWithClassName:_monsterClassString andState:MSWalk];
    self.actDie = [MonsterAnimationLoader loadAnimateWithClassName:_monsterClassString andState:MSDie];
    sprite = [CCSprite spriteWithFile:@"Monster/zombie/still/zombie_still01.png"];
    [self addChild:sprite];
    return self;
}

-(id)initWithDefaultMonsterInfoAndPosition:(CGPoint)pos andBehaviour:(SHZombieBehaviour*)b
{
    self = [self init];
    self.position = pos;
    self.myInfo = [[[MonsterActorInfo alloc] initWithPosition:pos andRotation:0 andRect:CGRectMake(0, 0, 50, 50) andGotType:gotDynamic andAttType:attMonster] autorelease];
    [[SHb2dCollisionManager sharedSHb2dCollisionManager] addBoxBodyWithCircleShapeForGameObjectWithInfo:_myInfo];
    self.behaviour = b;
    return self;
}

-(void)setBehaviour:(SHZombieBehaviour *)behaviour
{
    _behaviour = behaviour;
    [_behaviour addZombie:self];
}

-(SHZombieBehaviour*)behaviour
{
    return _behaviour;
}

-(void)onEnter
{
    [super onEnter];
    [self scheduleUpdate];
}

-(void)update:(ccTime)dt
{
    bool markDestroy = NO;
    for (GameActorInfo *info in _myInfo.contactsInfo)
    {
        if (info.attType == attProjectile)
        {
            // should be replaced with logic
            
            ProjectileActorInfo *projInfo = (ProjectileActorInfo*)info;
            _health -= projInfo.damage;
            
            if (_health <= 0)
            {
                // die
                markDestroy = YES;
            }
            
            
            //
        }
    }
    
    [_myInfo.contactsInfo removeAllObjects];
    
    
    if (!_isDestroyed)
    {
        // define if moving
        CGPoint mip = _myInfo.position;
        CGPoint prp = _previousPosition;
        CGPoint epsilon = ccp(1,1);
        if ([self comparePoint:mip andPoint:prp withEpsilon:epsilon])
            self.isMoving = NO;
        else
            self.isMoving = YES;
        
        // rotate
        float ang = ccpAngleSigned(ccp(0,1), self.velocityToTarget);
        sprite.rotation = CC_RADIANS_TO_DEGREES(-ang);
        
        _previousPosition = _myInfo.position;
        self.position = _myInfo.position;
        _myInfo.b2bvelocity = self.velocityToTarget;
        
        if (markDestroy)
        {
            [sprite runAction:_actDie];
            [self destroy];
            _isDestroyed = YES;
        }
    }
}

-(void)setIsMoving:(bool)isMoving
{
    _isMoving = isMoving;
    [self changeState:MSWalk];
}

-(bool)isMoving
{
    return _isMoving;
    
}

-(void)changeState:(MonsterState)incState
{
    if (incState != self.state)
    {
        self.state = incState;
        switch (incState) {
            case MSAttack:
                [sprite runAction:self.actAttack];
                break;
                
            case MSDie:
                [sprite runAction:self.actDie];
                break;
                
            case MSWalk:
                [sprite runAction:self.actWalk];
                break;
                
            case MSStill:
                [sprite runAction:self.actStill];
                break;
            default:
                break;
        }
    }
    
}

-(bool)comparePoint:(CGPoint)p1 andPoint:(CGPoint)p2 withEpsilon:(CGPoint)e
{
    return (fabsf(p1.x-p2.x)<e.x && fabsf(p1.y-p2.y)<e.y);
}

-(void)dealloc
{
    [_monsterClassString release];
    self.behaviour = nil;
    [super dealloc];
}

-(void)destroy
{
    //self.myInfo = nil; oh no! the info will stay, but the body will be removed!
    [_myInfo removePhysicsBody];
}

@end
