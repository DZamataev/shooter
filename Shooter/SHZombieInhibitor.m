//
//  SHZombieInhibitor.m
//  Anti-heroes Wars
//
//  Created by Denis on 4/9/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "SHZombieInhibitor.h"
#import "SHZombieBehaviour.h"
#import "SHZombie.h"


@implementation SHZombieInhibitor
@synthesize behaviour = _behaviour;
-(id)initWithSpawnRate:(float)sr andTargets:(NSArray*)targets andMaximumZombies:(int)maxZomb andLayer:(CCLayer*)layer;
{
    self = [super init];
    _behaviour = [[SHZombieBehaviour alloc] initWithTargets:targets];
    _layer = layer;
    _maximumZombies = maxZomb;
    _spawnRate = sr;
    _timeAccum = 0;
    return self;
}

-(void)onEnter
{
    [super onEnter];
    [self scheduleUpdate];
}

-(void)update:(ccTime)dt
{
    _timeAccum += dt;
    
    [_behaviour updateVelocities];
    
    if (_timeAccum >= _spawnRate)
    {
        _timeAccum = 0;
        if (_behaviour.aliveZombies < _maximumZombies)
        {
            SHZombie *zom = [[[SHZombie alloc] initWithDefaultMonsterInfoAndPosition:self.position andBehaviour:_behaviour] autorelease];
            [_layer addChild:zom];
        }
    }
}

-(void)dealloc
{
    self.behaviour = nil;
    [super dealloc];
}
@end
