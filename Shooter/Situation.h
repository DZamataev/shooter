//
//  Situation.h
//  Anti-heroes Wars
//
//  Created by Denis on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CWLSynthesizeSingleton.h"
@interface Situation : NSObject
{
    float _tileSizeDisplayMultiplier; // in AppDelegate we will determine it according to retina or not display we have
}
CWL_DECLARE_SINGLETON_FOR_CLASS(Situation)
@property float tileSizeDisplayMultiplier;
@end
