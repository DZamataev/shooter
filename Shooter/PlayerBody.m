//
//  PlayerBody.m
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "PlayerBody.h"
typedef enum {rotNotSet,rotClockwise,rotCounterclockwise}rotType;

@implementation PlayerBody
@synthesize normal = _normal, bodyJoystickIsTouched, shootingAngle = _shootingAngle, angularSpeed = _angularSpeed;
@synthesize stillBend = _stillBend;
@synthesize rightBend = _rightBend;
@synthesize leftBend = _leftBend;
-(id)init
{
    self = [super init];
    _joyPosition = ccp(0,0);
    sprite = [CCSprite spriteWithFile:@"Player/Body.png"];
    sprite.anchorPoint = ccp(0.5,0.5);
    [self addChild:sprite];
//    sprite.scale = 0.5f;
    _normal = ccp(0,1);
    _shootingAngle = 0;
    //sprite.opacity = 100;
    self.bodyJoystickIsTouched = NO;
    return self;
}

-(void)onEnter
{
    [super onEnter];
    [self scheduleUpdate];
}

-(void)runBendActionOnSprite:(CCAction *)action;
{
    [sprite runAction:action];
}

-(void)stopAllActionsOnSprite
{
    [sprite stopAllActions];
}

-(void)update:(ccTime)dt
{
    _angularSpeed = 300*dt;
    if (_joyPosition.x != 0 || _joyPosition.y != 0)
    {
        CGPoint pt = ccpSub(self.position, _joyPosition);
        CGPoint convPos = ccp(pt.x,-pt.y);
        float angle = fmodf(ccpAngleSigned(_normal, ccpNormalize(convPos))+M_PI, 2*M_PI)-M_PI;
        CGFloat fi = (angle*180/M_PI) ;
        float spr = fmodf(sprite.rotation+180,360)-180;
    
        float angToGo;
        
        float delta = spr - fi;
        if (fabsf(delta) < 180)
        {
            angToGo = - delta;
        }
        else
        {
            angToGo = (360-fabsf(delta))*(delta/fabsf(delta));
        }
        if (fabsf(angToGo) > _angularSpeed)
        {
            fi = spr + (angToGo/fabsf(angToGo))*_angularSpeed;
        }
        
        sprite.rotation = fi;
        _shootingAngle = -fi;
        //[super setPosition:position];
        
        // dont forget to make joyPos zero
        _joyPosition = CGPointZero;
    }
}

-(void)setPosition:(CGPoint)position
{
    if (_joyPosition.x == 0 || _joyPosition.y == 0)
        _joyPosition = position;
}

-(void)repositionBody:(CGPoint)position
{
    [super setPosition:position];
}

-(void)dealloc
{
    self.stillBend = nil;
    self.rightBend = nil;
    self.leftBend = nil;
    [super dealloc];
}

@end
