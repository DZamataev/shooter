//
//  Base.h
//  Anti-heroes Wars
//
//  Created by Denis on 4/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class SHBaseHealth;
@class GameActorInfo;
@class BaseActorInfo;

@interface Base : CCNode {
    NSMutableArray *_tilesInfo;
    CCSprite *_sprite;
    SHBaseHealth *_health;
}
@property (nonatomic, assign) SHBaseHealth *health;
@property (nonatomic, retain) NSMutableArray *tilesInfo;

-(void)pushTileInfo:(BaseActorInfo*)info;
@end
