//
//  SHBox2dBodyHolder.mm
//  Anti-heroes Wars
//
//  Created by Denis on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SHBox2dBodyHolder.h"
#import "cocos2d.h"

@implementation SHBox2dBodyHolder
@synthesize theB2Body = _theB2Body;
@synthesize theWorld = _theWorld;
-(id)init
{
    self = [super init];
    _theB2Body = nil;
    _theWorld = nil;
    return self;
}

-(void)purgeStoredData
{
    if (_theB2Body != nil && _theWorld != nil)
    {
        _theWorld->DestroyBody(_theB2Body);
        CCLOG(@"b2world destroyed the body on purgeData");
    }
    else {
        CCLOG(@"###no b2Body destroyed on purgeData");
    }
    _theWorld = nil;
    _theB2Body = nil;
}

-(void)dealloc
{
    _theWorld = nil;
    _theB2Body = nil;
    [super dealloc];
}
@end
