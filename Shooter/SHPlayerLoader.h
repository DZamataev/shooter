//
//  SHPlayerLoader.h
//  Anti-heroes Wars
//
//  Created by Denis on 4/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "AnimationLoader.h"
#import "CCAnimate+SequenceLoader.h"
#import "CCAnimation+SequenceLoader.h"

@class SHPlayerWeapon;

typedef enum {SHPAFSTRight,SHPAFSTLeft, SHPAFSTStill}SHPlayerAnimationFormatSideType;
typedef enum {SHPAFCTLegs, SHPAFCTBody} SHPlayerAnimationFormatCorpseType;
struct SHPlayerAnimationFormatData {
    SHPlayerAnimationFormatSideType sideType;
    SHPlayerAnimationFormatCorpseType corpseType;
};
@interface SHPlayerLoader : NSObject
{
    NSString *_modelName;
    NSString *_colorProperty;
    SHPlayerWeapon *_startWeapon;
}
@property (nonatomic, retain) NSString *modelName;
@property (nonatomic, retain) NSString *colorProperty;
@property (nonatomic, retain) SHPlayerWeapon *startWeapon;


-(id)initWithModelName:(NSString*)mName andColorProperty:(NSString*)cProp andStartWeapon:(SHPlayerWeapon*)sWeapon;
-(NSString*)getAnimationFormatWithSHPlayerAnimationFormatData:(struct SHPlayerAnimationFormatData)shpafd;
-(NSString*)getDatafileName;
@end
