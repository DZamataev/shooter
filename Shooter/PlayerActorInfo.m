//
//  PlayerActorInfo.m
//  Shooter
//
//  Created by Denis on 3/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PlayerActorInfo.h"

@implementation PlayerActorInfo
@synthesize forcedPosition = _forcedPosition;
@synthesize safePoint = _safePoint;
@synthesize contactsInfo = _contactsInfo;
@synthesize collisionWorkedOutAndSolved = _collisionWorkedOutAndSolved;
-(id)initWithPosition:(CGPoint)pos andRotation:(CGFloat)rotation andRect:(CGRect)rect andGotType:(gameObjectType)got andAttType:(attachedToType)att
{
    self = [super initWithPosition:pos andRotation:rotation andRect:rect andGotType:got andAttType:att];
    
    _forcedPosition = ccp(-1,-1);
    _safePoint = pos;
    _contactsInfo = [NSMutableArray new];
    
    return self;
}

-(void)pushContact:(GameActorInfo*)contactInfo
{
    [_contactsInfo addObject:contactInfo];
}

-(void)dealloc
{
    [_contactsInfo release];
    [super dealloc];
}
@end
