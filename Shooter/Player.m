//
//  Player.m
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Player.h"
#import "ViewportController.h"
#import "SHPvPLogic.h"
#import "SHPlayerLoader.h"
#import "SHPlayerBehavior.h"

@implementation Player
@synthesize body = _body, legs = _legs, startPoint = _startPoint, startAngle = _startAngle;
@synthesize myInfo = _myInfo;
@synthesize currentWeapon = _currentWeapon;
-(id)initWithLoader:(SHPlayerLoader*)ploader
{
    self = [super init];

    
    // test init the loader
    //struct SHPlayerAnimationFormatData animFormatData;
    
    _playerLoader = ploader;
    
    _body = [[[[PlayerBody alloc] init] autorelease] retain];
    [self addChild:_body z:2];
    
    _legs = [[[[PlayerLegs alloc] initWithBody:_body] autorelease] retain];
    [self addChild:_legs z:1];

    _startPoint = ccp(0,0);
    _startAngle = 0;
    
    // setup the weapon
    self.currentWeapon = ploader.startWeapon;
    
    // setup a node in viewport
    if ([ViewportController sharedViewportController].firstNode == nil)
    {
        [ViewportController sharedViewportController].firstNode = _legs;
    }
    else if ([ViewportController sharedViewportController].secondNode == nil)
    {
        [ViewportController sharedViewportController].secondNode = _legs;
    }
    
    // setup self for PvPLogic
    if ([SHPvPLogic sharedSHPvPLogic].firstPlayer == nil)
    {
        [SHPvPLogic sharedSHPvPLogic].firstPlayer = self;
    }
    else if ([SHPvPLogic sharedSHPvPLogic].secondPlayer == nil)
    {
        [SHPvPLogic sharedSHPvPLogic].secondPlayer = self;
    }
    
    // load actions to legs and body
    _legs.stillStep = [self loadStillStepForLegs];
    _legs.leftStep = [self loadLeftStepForLegs];
    _legs.rightStep = [self loadRightStepForLegs];
    
    _body.stillBend = [self loadStillBendForBody];
    _body.leftBend = [self loadLeftBendForBody];
    _body.rightBend = [self loadRightBendForBody];
    
    // init the behavior for managing actions
    _behavior = [[SHPlayerBehavior alloc] initWithPlayer:self andLegs:_legs andBody:_body];
    _legs.movementDelegate = _behavior;
    
    return self;
}

-(void)teleportToPosition:(CGPoint)pos
{
    _myInfo.forcedPosition = pos;
    [_legs repositionLegs:pos];
    [_body repositionBody:pos];
}

-(void)onEnter
{
    [super onEnter];
    [_legs repositionLegs:_startPoint];
    [_body repositionBody:_startPoint];
    [self setupPhysics];
    [self scheduleUpdate];
    
}

-(void)update:(ccTime)dt
{
    for (GameActorInfo *info in _myInfo.contactsInfo)
    {
        if (info.attType == attProjectile)
        {
            [[SHPvPLogic sharedSHPvPLogic] player:self gotHitByProjectile:(ProjectileActorInfo*)info];
        }
    }
    [_myInfo.contactsInfo removeAllObjects];
}
     
-(void)setupPhysics
{
    _myInfo = [_legs setupPhysicsBody];
}

-(CCAnimate*)loadStillStepForLegs
{
    CCAnimation* anim = nil;
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:[_playerLoader getDatafileName]];
    
    struct SHPlayerAnimationFormatData formatData;
    formatData.corpseType = SHPAFCTLegs;
    formatData.sideType = SHPAFSTStill;
    NSString *add = @"%02d.png";
    NSString *fullFormat = [NSString stringWithFormat:@"%@%@",[_playerLoader getAnimationFormatWithSHPlayerAnimationFormatData:formatData],add];
    anim = [CCAnimation animationWithSpriteSequence:fullFormat delay:0.1];
    
    return [CCAnimate actionWithAnimation:anim];
}

-(CCAnimate*)loadLeftStepForLegs
{
    CCAnimation* anim = nil;
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:[_playerLoader getDatafileName]];
    
    struct SHPlayerAnimationFormatData formatData;
    formatData.corpseType = SHPAFCTLegs;
    formatData.sideType = SHPAFSTLeft;
    NSString *add = @"%02d.png";
    NSString *fullFormat = [NSString stringWithFormat:@"%@%@",[_playerLoader getAnimationFormatWithSHPlayerAnimationFormatData:formatData],add];
    anim = [CCAnimation animationWithSpriteSequence:fullFormat delay:0.1];

    return [CCAnimate actionWithAnimation:anim];
}

-(CCAnimate*)loadRightStepForLegs
{
    CCAnimation* anim = nil;
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:[_playerLoader getDatafileName]];
    
    struct SHPlayerAnimationFormatData formatData;
    formatData.corpseType = SHPAFCTLegs;
    formatData.sideType = SHPAFSTRight;
    
    NSString *add = @"%02d.png";
    NSString *fullFormat = [NSString stringWithFormat:@"%@%@",[_playerLoader getAnimationFormatWithSHPlayerAnimationFormatData:formatData],add];
    anim = [CCAnimation animationWithSpriteSequence:fullFormat delay:0.1];
    
    return [CCAnimate actionWithAnimation:anim];
}

-(CCAnimate*)loadStillBendForBody
{
    CCAnimation* anim = nil;
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:[_playerLoader getDatafileName]];
    
    struct SHPlayerAnimationFormatData formatData;
    formatData.corpseType = SHPAFCTBody;
    formatData.sideType = SHPAFSTStill;
    NSString *add = @"%02d.png";
    NSString *fullFormat = [NSString stringWithFormat:@"%@%@",[_playerLoader getAnimationFormatWithSHPlayerAnimationFormatData:formatData],add];
    anim = [CCAnimation animationWithSpriteSequence:fullFormat delay:0.1];
    
    return [CCAnimate actionWithAnimation:anim];
}

-(CCAnimate*)loadLeftBendForBody
{
    CCAnimation* anim = nil;
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:[_playerLoader getDatafileName]];
    
    struct SHPlayerAnimationFormatData formatData;
    formatData.corpseType = SHPAFCTBody;
    formatData.sideType = SHPAFSTLeft;
    NSString *add = @"%02d.png";
    NSString *fullFormat = [NSString stringWithFormat:@"%@%@",[_playerLoader getAnimationFormatWithSHPlayerAnimationFormatData:formatData],add];
    anim = [CCAnimation animationWithSpriteSequence:fullFormat delay:0.1];
    
    return [CCAnimate actionWithAnimation:anim];
}

-(CCAnimate*)loadRightBendForBody
{
    CCAnimation* anim = nil;
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:[_playerLoader getDatafileName]];
    
    struct SHPlayerAnimationFormatData formatData;
    formatData.corpseType = SHPAFCTBody;
    formatData.sideType = SHPAFSTRight;
    NSString *add = @"%02d.png";
    NSString *fullFormat = [NSString stringWithFormat:@"%@%@",[_playerLoader getAnimationFormatWithSHPlayerAnimationFormatData:formatData],add];
    anim = [CCAnimation animationWithSpriteSequence:fullFormat delay:0.1];
    
    return [CCAnimate actionWithAnimation:anim];
}

-(CGPoint)getTargetPosition
{
    return _legs.position;
}

-(void)dealloc
{
    self.currentWeapon = nil;
    [_playerLoader release];
    [super dealloc];
}
@end
