//
//  ViewportController.h
//  Shooter
//
//  Created by Denis on 3/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CWLSynthesizeSingleton.h"
#import "cocos2d.h"
#import "GameActorInfo.h"
@protocol ViewportControllerProtocol

-(void)setViewpointCenter:(CGPoint) position;

@end

@interface ViewportController : NSObject
{
    GameActorInfo *_myInfo;
    CCNode *_firstNode;
    CCNode *_secondNode;
    CGPoint _centerOfViewport;
    CGPoint _centerOfViewportOnLayer;
    CCLayer <ViewportControllerProtocol> *_layer;
    CGRect _viewportRect;
    float _layerScale;
    
    float _addingOnScaleX;
    float _addingOnScaleY;
    
    BOOL _customViewportCenterSet;
    BOOL _customViewportRectSet;
}
CWL_DECLARE_SINGLETON_FOR_CLASS(ViewportController)
@property (nonatomic, assign) CCNode *firstNode;
@property (nonatomic, assign) CCNode *secondNode;
@property (nonatomic, assign) CCNode *layer;
@property CGPoint centerOfViewport;
@property CGRect viewportRect;
@property CGPoint centerOfViewportOnLayer;
@property float layerScale;
@property (nonatomic, retain) GameActorInfo *myInfo;
@property float addingOnScaleX;
@property float addingOnScaleY;

-(void)toggleAutomaticViewportCenter;
-(void)toggleAutomaticViewportRect;

@end
