//
//  SHPlayerLoader.m
//  Anti-heroes Wars
//
//  Created by Denis on 4/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SHPlayerLoader.h"

@implementation SHPlayerLoader
@synthesize modelName = _modelName;
@synthesize colorProperty = _colorProperty;
@synthesize startWeapon = _startWeapon;
-(id)initWithModelName:(NSString*)mName andColorProperty:(NSString*)cProp andStartWeapon:(SHPlayerWeapon *)sWeapon
{
    self = [super init];
    self.modelName = mName;
    self.colorProperty = cProp;
    self.startWeapon = sWeapon;
    return self;
}

-(NSString*)getDatafileName
{
    NSString *result = [NSString stringWithFormat:@"Player/%@/%@/%@_%@.plist",_modelName,_colorProperty,_modelName,_colorProperty];
    return result;
}

-(NSString*)getAnimationFormatWithSHPlayerAnimationFormatData:(struct SHPlayerAnimationFormatData)shpafd
{
    NSString *leftOrRightMaybeStill = nil;
    NSString *bodyOrLegs = nil;
    switch (shpafd.corpseType) {
        case SHPAFCTBody:
            bodyOrLegs = [NSString stringWithFormat:@"body"];
            break;
            
        case SHPAFCTLegs:
            bodyOrLegs = [NSString stringWithFormat:@"legs"];
            break;
            
        default:
            break;
    }
    
    switch (shpafd.sideType) {
        case SHPAFSTLeft:
            leftOrRightMaybeStill = [NSString stringWithFormat:@"L"];
            break;
            
        case SHPAFSTRight:
            leftOrRightMaybeStill = [NSString stringWithFormat:@"R"];
            break;
            
        case SHPAFSTStill:
            leftOrRightMaybeStill = [NSString stringWithFormat:@"S"];
            break;
        default:
            break;
    }
    
    NSString *result = [NSString stringWithFormat:@"%@_%@_%@_%@_",_modelName,_colorProperty,bodyOrLegs,leftOrRightMaybeStill];
    return result;
}

-(void)dealloc
{
    self.modelName = nil;
    self.colorProperty = nil;
    self.startWeapon = nil;
    [super dealloc];
}
@end
