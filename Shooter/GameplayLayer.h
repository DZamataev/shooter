//
//  GameplayLayer.h
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//



#import "cocos2d.h"

#import "PlayerController.h"

#import "Player.h"
#import "PlayerShooting.h"
#import "CollisionManager.h"

#import "HUDLayer.h"

#import "ViewportController.h"

#import "SHTileMapObjectProcessor.h"
@class SHb2dCollisionManager;
@class SHPvPLogic;
@class Base;
@class SHBaseHealth;
@class SDrawableLayer;
@class SHZombieInhibitor;

#define FIRST_BASE_LABEL_COLOR 20,20,170
#define SECOND_BASE_LABEL_COLOR 20,170,20

@interface GameplayLayer  : CCLayer <ViewportControllerProtocol> 
{
    HUDLayer *_HUDLayer;
    Player *player1;
    Player *player2;
    
    PlayerController *p1controller;
    PlayerController *p2controller;

    PlayerShooting *p1shooter;
    PlayerShooting *p2shooter;
    
    NSMutableArray *_projectiles;
    
    float _timeAccum;
    
    bool _slowMoToggle;
    float _slowMoTimeAccum;
    float _slowMoTimeScaleTargetValue;
    float _slowMoTimeScaleBeginValue;
    
    CCTMXTiledMap *_tileMap;
    CCTMXLayer *_background;
    CCTMXLayer *_objects;
    CCTMXLayer *_meta;
    
    struct SHTileMapObjectsInfo _tileMapObjectsInfo;
    
    CollisionManager *_collisionManager;
    SHb2dCollisionManager *_b2dColMan;

    SHPvPLogic *_PvPLogic;
    
    // Base health display? - take it from pvplogic
    // and update labels from it
    Base *base1;
    Base *base2;
    CCLabelTTF *_firstBaseLabel;
    CCLabelTTF *_secondBaseLabel;
    
    SDrawableLayer *_myHaloLayer;
    
    // test zombie count
    SHZombieInhibitor *_testZombieInhibitor;
    
}
@property (nonatomic, retain) NSMutableArray *projectiles;
@property bool isOnSlowMo;

@property (nonatomic, assign) HUDLayer *HUDLayer;

@property (nonatomic, retain) CCTMXTiledMap *tileMap;
@property (nonatomic, retain) CCTMXLayer *background;
@property (nonatomic, retain) CCTMXLayer *objects;
@property (nonatomic, retain) CCTMXLayer *meta;
@property (nonatomic, retain) SDrawableLayer *myHaloLayer;

@property (nonatomic, assign) SHPvPLogic *PvPLogic;


+(CCScene *) scene;
-(void)readObjects;
-(void)initLogic;
-(void)initPlayers;
-(void)initBases;
-(void)initCollisionManager;

-(void)update:(ccTime)dt;

- (float)tileMapHeight;
- (float)tileMapWidth;

-(void)doSlowMo;
-(void)undoSlowMo;

-(void)doChangeScale:(float)incScale;

-(void)b2dCollisionSimulation:(ccTime)dt;

//-(void)setViewpointCenter:(CGPoint) position;

@end
