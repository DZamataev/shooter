//
//  TestLayer.h
//  SomeShooter
//
//  Created by Denis on 3/17/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GameplayLayer.h"

@interface TestLayer : CCLayer {
    GameplayLayer *_gameplayLayer;
}
+(CCScene *) scene;
@end
