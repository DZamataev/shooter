//
//  CollisionManager.m
//  Shooter
//
//  Created by Denis on 3/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CollisionManager.h"
#import "SHMetaTile.h"

@implementation CollisionManager
@synthesize tileMap = _tileMap;
@synthesize meta = _meta;

CWL_SYNTHESIZE_SINGLETON_FOR_CLASS(CollisionManager)

-(void)gatherCollidableTiles
{
    NSMutableArray *collidableTiles = [NSMutableArray new];
    for (NSUInteger y=0; y<_meta.layerSize.height; y++)
    {
        for (NSUInteger x=0; x<_meta.layerSize.width; x++)
        {
            NSUInteger pos = x + _meta.layerSize.width * y;
            uint32_t tileGid = _meta.tiles[pos];
            if (tileGid>0) {
                NSDictionary *properties = [_tileMap propertiesForGID:tileGid];
                if (properties) {
                    NSString *collision = [properties valueForKey:@"Collidable"];
                    if (collision && [collision compare:@"True"] == NSOrderedSame) {
                        // if collidable property is "True"
                        SHMetaTile *tile = [[SHMetaTile alloc] initWithGid:tileGid andPosition:ccp(x*_tileMap.tileSize.width+_tileMap.tileSize.width/2, (((_tileMap.mapSize.height-1) - y) * _tileMap.tileSize.height) - _tileMap.tileSize.height/2)];
                        [collidableTiles addObject:tile];
                    }
                }
            }
        }
    }
    
    [_collidableTiles release];
    _collidableTiles = [[NSArray arrayWithArray:collidableTiles] retain];
    [collidableTiles release];
    CCLOG(@"Gather collidable tiles ended with count: %i",[_collidableTiles count]);
    
}

#pragma mark -
#pragma mark Check methods

-(bool)checkCollisionForPosition:(CGPoint)position
{
    if (![self screenContainsPosition:position]) // check for out of screen
        return YES;
    
    CGPoint tileCoord = [self tileCoordForPosition:position];
    int tileGid = [_meta tileGIDAt:tileCoord];
    if (tileGid) {
        NSDictionary *properties = [_tileMap propertiesForGID:tileGid];
        if (properties) {
            NSString *collision = [properties valueForKey:@"Collidable"];
            if (collision && [collision compare:@"True"] == NSOrderedSame) {
                return YES;
            }
        }
    }
    return NO;
}

-(NSArray*)checkCollisionsTilesPositionsForRect:(CGRect)rect
{    
    NSMutableArray *tilesPositions = [NSMutableArray new];
    for (SHMetaTile *tile in _collidableTiles)
    {
        CGPoint tilePos = tile.mapPosition;
        CGRect tileRect = CGRectMake(tilePos.x-_tileMap.tileSize.width/2,
                                     tilePos.y-_tileMap.tileSize.height/2,
                                     _tileMap.tileSize.width,
                                     _tileMap.tileSize.height);
        if (CGRectIntersectsRect(rect, tileRect))
        {
            [tilesPositions addObject:[NSValue valueWithCGPoint:tilePos]];
        }
    }
    return tilesPositions;
}

-(NSArray*)checkCollisionsTilesPositionsForRect:(CGRect)rect withTilesPositionsArray:(NSArray*)posArray
{
    NSMutableArray *tilesPositions = [NSMutableArray new];
    for (NSValue *pointValue in posArray)
    {
        CGPoint pos = pointValue.CGPointValue;
        float tileX = pos.x;
        float tileY = pos.y;
        CGRect tileRect = CGRectMake(tileX-_tileMap.tileSize.width/2,
                                     tileY-_tileMap.tileSize.height/2,
                                     _tileMap.tileSize.width,
                                     _tileMap.tileSize.height);
        if (CGRectIntersectsRect(rect, tileRect))
        {
            // if intersection took place
            [tilesPositions addObject:[NSValue valueWithCGPoint:pos]];
        }
    }
    return tilesPositions;
}

-(bool)checkCollisionForTilesPositionsArray:(NSArray*)posArray withRect:(CGRect)rect
{
    for (NSValue *pointValue in posArray)
    {
        CGPoint pos = pointValue.CGPointValue;
        float tileX = pos.x;
        float tileY = pos.y;
        CGRect tileRect = CGRectMake(tileX-_tileMap.tileSize.width/2,
                                     tileY-_tileMap.tileSize.height/2,
                                     _tileMap.tileSize.width,
                                     _tileMap.tileSize.height);
        if (CGRectIntersectsRect(rect, tileRect))
        {
            // if intersection took place
            return YES;
        }
    }
    return NO;
}

-(NSArray*)getTilesPosistionsNearbyForRect:(CGRect)rect withMultiplier:(float)mult
{
    NSMutableArray *nearbyTilesPositions = [NSMutableArray new];
    float radiusForRect;
    if (rect.size.width>rect.size.height) radiusForRect = rect.size.width*mult;
    else
        radiusForRect = rect.size.height*mult;
    CGPoint rectCenter = ccp(rect.origin.x+rect.size.width/2,rect.origin.y+rect.size.height/2);
    for (SHMetaTile *tile in _collidableTiles)
    {
        if ([self point:rectCenter intersectsCircleWithOrigin:tile.mapPosition andRadius:radiusForRect])
        {
            [nearbyTilesPositions addObject:[NSValue valueWithCGPoint:tile.mapPosition]];
        }
    }
    return nearbyTilesPositions;
}

#pragma mark - 
#pragma mark Useful methods

- (CGPoint)tileCoordForPosition:(CGPoint)position {
    int x = position.x / _tileMap.tileSize.width;
    int y = ((_tileMap.mapSize.height * _tileMap.tileSize.height) - position.y) / _tileMap.tileSize.height;
    return ccp(x, y);
}

-(bool)screenContainsPosition:(CGPoint)position
{
    CGSize winSize = [CCDirector sharedDirector].winSize;
    CGRect screenRect = CGRectMake(0, 0, winSize.width, winSize.height);
    if (!CGRectContainsPoint(screenRect, position))
    {
        return NO;
    }
    return YES;
}

-(bool)point:(CGPoint)p1 intersectsCircleWithOrigin:(CGPoint)p2 andRadius:(CGFloat)rad
{
    CGFloat dx = p1.x-p2.x;
    CGFloat dy = p1.y-p2.y;
    return (rad*rad)>(dx*dx)+(dy*dy);
}

-(CGRect)calculateRectForPosition:(CGPoint)position andSprite:(CCSprite *)sprite
{
    CGSize spriteBBoxSize = sprite.boundingBox.size;
    return [self calculateRectForPosition:position andSize:spriteBBoxSize];
}

-(CGRect)calculateRectForPosition:(CGPoint)position andSize:(CGSize)size
{
    return CGRectMake(position.x-size.width/2,
                      position.y-size.height/2,
                      size.width,
                      size.height);
}
@end
