//
//  MonsterAnimationLoader.m
//  Anti-heroes Wars
//
//  Created by Denis on 4/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MonsterAnimationLoader.h"
#import "CCAnimate+SequenceLoader.h"
#import "CCAnimation+SequenceLoader.h"

@implementation MonsterAnimationLoader
+(CCAnimate*)loadAnimateWithClassName:(NSString*)className andState:(MonsterState)stateName
{
    CCAnimation* anim = nil;
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:[NSString stringWithFormat:@"Monster/%@/%@.plist",className,className]];
    
    NSString *stateString;
    switch (stateName) {
        case MSStill:
            stateString = @"still";
            break;
            
        case MSWalk:
            stateString = @"walk";
            break;
            
        case MSAttack:
            stateString = @"walk";
            break;
            
        case MSDie:
            stateString = @"die";
            break;
            
        default:
            break;
    }
    NSString *add = @"%02d.png";
    NSString *fullFormat = [NSString stringWithFormat:@"%@_%@%@",className,stateString,add];
    anim = [CCAnimation animationWithSpriteSequence:fullFormat delay:0.1];
    
    return [CCAnimate actionWithAnimation:anim];
}
@end
