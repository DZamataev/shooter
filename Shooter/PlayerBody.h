//
//  PlayerBody.h
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface PlayerBody : CCNode {
    CCAnimate *_stillBend;
    CCAnimate *_leftBend;
    CCAnimate *_rightBend;
    CCSprite *sprite;
    float _shootingAngle;
    CGPoint _normal;
    CGPoint prevPos;
    CGPoint _joyPosition;
    float _angularSpeed;
}
@property CGPoint normal;
@property bool bodyJoystickIsTouched;
@property float shootingAngle;
@property float angularSpeed;
@property (nonatomic, retain) CCAnimate *stillBend;
@property (nonatomic, retain) CCAnimate *leftBend;
@property (nonatomic, retain) CCAnimate *rightBend;
-(id)init;
-(void)setPosition:(CGPoint)position;
-(void)repositionBody:(CGPoint)position;
-(void)update:(ccTime)dt;
-(void)runBendActionOnSprite:(CCAction*)action;
-(void)stopAllActionsOnSprite;
@end
