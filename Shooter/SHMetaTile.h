//
//  SHMetaTile.h
//  Shooter
//
//  Created by Denis on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SHMetaTile : NSObject
{
    uint32_t _gid;
    CGPoint _mapPosition;
}
@property uint32_t gid;
@property CGPoint mapPosition;

-(id)initWithGid:(uint32_t)incGid andPosition:(CGPoint)incPos;
@end
