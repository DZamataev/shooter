//
//  PlayerShooting.mm
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PlayerShooting.h"
#import "Projectile.h"
#import "GameplayLayer.h"
#import "SHb2dCollisionManager.h"
#import "Player.h"
#import "SHPlayerWeapon.h"

@implementation PlayerShooting
-(id)initWithPlayerControlledBody:(PlayerBody *)bod andLayer:(CCLayer *)lay andPlayer:(Player*)player andFireports:(NSMutableArray*)fports;
{
    self = [super init];
    _body = bod;
    _layer = lay;
    _player = player;
    _firePorts = fports;
    [self schedule:@selector(tick:)];
    _timeAccum = 0;
    _projectileArmed = NO;
    return self;
}

-(void)tick:(float)dt
{
    _timeAccum+=dt;
    if (_timeAccum > _player.currentWeapon.characteristics.firingRate)
    {
        _timeAccum = 0;
        _projectileArmed = YES;
    }
    
    if (_projectileArmed && _body.bodyJoystickIsTouched)
    {
        // shoot projectile
        
        Projectile *proj = [[[Projectile alloc] initWithType:_player.currentWeapon.characteristics.typeOfProjectile andSpeedMod:_player.currentWeapon.characteristics.speed withAngle:_body.shootingAngle onLayer:_layer] autorelease];

        [_layer addChild:proj];
        CGPoint fPort = [[_firePorts objectAtIndex:0] CGPointValue];
        CGPoint firePortPos = ccpRotateByAngle(fPort, ccp(0,0), CC_DEGREES_TO_RADIANS(_body.shootingAngle));
        proj.position = ccpAdd(_body.position, firePortPos);
        
        ProjectileActorInfo *projInfo = [[[ProjectileActorInfo alloc] initWithPosition:proj.position andRotation:0 andRect:CGRectMake(0, 0, _player.currentWeapon.characteristics.collisionRadius, _player.currentWeapon.characteristics.collisionRadius) andGotType:gotDynamic andAttType:attProjectile] autorelease];
        projInfo.bounces = _player.currentWeapon.characteristics.bounces;
        projInfo.penetration = _player.currentWeapon.characteristics.penetration;
        projInfo.hostObject = _player;
        projInfo.damage = _player.currentWeapon.characteristics.damage;
        [[SHb2dCollisionManager sharedSHb2dCollisionManager] addBoxBodyWithCircleShapeForGameObjectWithInfo:projInfo];
        proj.myInfo = projInfo;
        //
        
        _projectileArmed = NO;
        _timeAccum = 0;
    }
}
@end
