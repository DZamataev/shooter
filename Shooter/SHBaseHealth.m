//
//  SHBaseHealth.m
//  Anti-heroes Wars
//
//  Created by Denis on 4/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SHBaseHealth.h"

@implementation SHBaseHealth
@synthesize intValue = _intValue;
+(id)SHBaseHealthWithInt:(int)value
{
    SHBaseHealth *object = [[SHBaseHealth alloc] init];
    object.intValue = value;
    return object;
}
@end
