//
//  SHPlayerHealth.m
//  Anti-heroes Wars
//
//  Created by Denis on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SHPlayerHealth.h"

@implementation SHPlayerHealth
@synthesize intValue = _intValue;
+(id)SHPlayerHealthWithInt:(int)value
{
    SHPlayerHealth *object = [[SHPlayerHealth alloc] init];
    object.intValue = value;
    return object;
}
@end
