//
//  BaseActorInfo.m
//  Anti-heroes Wars
//
//  Created by Denis on 4/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BaseActorInfo.h"

@implementation BaseActorInfo
@synthesize contactsInfo = _contactsInfo;
-(id)initWithPosition:(CGPoint)pos andRotation:(CGFloat)rotation andRect:(CGRect)rect andGotType:(gameObjectType)got andAttType:(attachedToType)att
{
    self = [super initWithPosition:pos andRotation:rotation andRect:rect andGotType:got andAttType:att];
    
    _contactsInfo = [NSMutableArray new];
    
    return self;
}

-(void)pushContact:(GameActorInfo*)contactInfo
{
    [_contactsInfo addObject:contactInfo];
}

-(void)dealloc
{
    [_contactsInfo release];
    [super dealloc];
}
@end
