//
//  SHZombieBehaviour.m
//  Anti-heroes Wars
//
//  Created by Denis on 4/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SHZombieBehaviour.h"
#import "SHZombie.h"
#import "SHMonsterTargetProtocol.h"

@implementation SHZombieBehaviour
-(id)initWithTargets:(NSArray *)t
{
    self = [super init];
    // check the targets
    for (id targ in t)
    {
        NSAssert([targ conformsToProtocol:@protocol(SHMonsterTargetProtocol)], @"Behaviour got target that does not conform to necessery protocol");
    }
    _targets = [NSMutableArray new];
    [_targets addObjectsFromArray:t];
    _zombies = [NSMutableArray new];
    return self;
}

-(void)addZombie:(SHZombie*)zom
{
    [_zombies addObject:zom];
}

-(void)updateVelocities
{
    for (SHZombie *zomb in _zombies)
    {
        CGPoint zombiePosition = zomb.position;
        CGPoint nearestTarget = [self nearestTargetPositionForPoint:zombiePosition];
        nearestTarget = ccpSub(nearestTarget, zombiePosition);
        float angle = ccpAngle(ccp(0,1), nearestTarget);
        if (nearestTarget.x<0) angle*=-1;
        CGPoint vect = ccp(zomb.speedMod*sin(angle),zomb.speedMod*cos(angle));
        zomb.velocityToTarget = vect;
//        NSLog(@"Zombie got vect: %f,%f",vect.x,vect.y);
    }
}

-(CGPoint)nearestTargetPositionForPoint:(CGPoint)position
{
    NSMutableArray *targetsPositions = [[NSMutableArray new] autorelease];
    for (id<SHMonsterTargetProtocol> targ in _targets)
    {
        [targetsPositions addObject:[NSValue valueWithCGPoint:[targ getTargetPosition]]];
    }
    [targetsPositions sortUsingComparator:  
     ^NSComparisonResult (id obj1, id obj2) {  
         NSValue *val1 = (NSValue*)obj1;
         NSValue *val2 = (NSValue*)obj2;
         CGPoint pt1 = val1.CGPointValue;
         CGPoint pt2 = val2.CGPointValue;  
         float distForPt1 = ccpDistance(position, pt1);
         float distForPt2 = ccpDistance(position, pt2);
         if (distForPt1 > distForPt2)  
             return NSOrderedDescending;  
         else if (distForPt1 < distForPt2)  
             return NSOrderedAscending;  
         else  
             return NSOrderedSame;  
     }  
     ];  
    CGPoint nearestTargetPos = ((NSValue*)([targetsPositions objectAtIndex:0])).CGPointValue;
    return nearestTargetPos;
}

-(int)aliveZombies
{
    int count = 0;
    for (SHZombie *z in _zombies)
    {
        if (!z.isDestroyed)
        {
            count++;
        }
    }
    return count;
}



-(void)dealloc
{
    [_targets release];
    [_zombies release];
    [super dealloc];
}


@end
