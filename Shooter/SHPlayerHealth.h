//
//  SHPlayerHealth.h
//  Anti-heroes Wars
//
//  Created by Denis on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SHPlayerHealth : NSObject
{
    int _intValue;
}
@property int intValue;
+(id)SHPlayerHealthWithInt:(int)value;
@end
