//
//  CollisionManager.h
//  Shooter
//
//  Created by Denis on 3/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CWLSynthesizeSingleton.h"

@interface CollisionManager : NSObject
{
    CCTMXTiledMap *_tileMap;
    CCTMXLayer *_meta;
    NSArray *_collidableTiles;
}
@property (nonatomic, retain) CCTMXTiledMap *tileMap;
@property (nonatomic, retain) CCTMXLayer *meta;

CWL_DECLARE_SINGLETON_FOR_CLASS(CollisionManager)


-(void)gatherCollidableTiles;

-(bool)checkCollisionForPosition:(CGPoint)position;
-(NSArray*)checkCollisionsTilesPositionsForRect:(CGRect)rect;
-(bool)checkCollisionForTilesPositionsArray:(NSArray*)posArray withRect:(CGRect)rect;
-(NSArray*)checkCollisionsTilesPositionsForRect:(CGRect)rect withTilesPositionsArray:(NSArray*)posArray;
-(NSArray*)getTilesPosistionsNearbyForRect:(CGRect)rect withMultiplier:(float)mult;

- (CGPoint)tileCoordForPosition:(CGPoint)position;
-(bool)screenContainsPosition:(CGPoint)position;
-(bool)point:(CGPoint)p1 intersectsCircleWithOrigin:(CGPoint)p2 andRadius:(CGFloat)rad;
-(CGRect)calculateRectForPosition:(CGPoint)position andSprite:(CCSprite*)sprite;
-(CGRect)calculateRectForPosition:(CGPoint)position andSize:(CGSize)size;
@end
