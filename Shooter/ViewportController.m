//
//  ViewportController.m
//  Shooter
//
//  Created by Denis on 3/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewportController.h"
#define VIEWPORT_RECT_CLIPPING_COEFF 1.0f

@implementation ViewportController
@synthesize myInfo = _myInfo;
@synthesize firstNode = _firstNode;
@synthesize secondNode = _secondNode;
@synthesize layer = _layer;
@synthesize layerScale = _layerScale;
@synthesize addingOnScaleX = _addingOnScaleX;
@synthesize addingOnScaleY = _addingOnScaleY;
CWL_SYNTHESIZE_SINGLETON_FOR_CLASS(ViewportController)
-(id)init
{
    self = [super init];
    _firstNode = nil;
    _secondNode = nil;
    _customViewportRectSet = _customViewportCenterSet = NO;
    CGSize winSize = [CCDirector sharedDirector].winSize;
    _centerOfViewport = _centerOfViewportOnLayer = ccp(winSize.width/2,winSize.height/2);
    _layerScale = 1;
    return self;
}

-(void)setCenterOfViewport:(CGPoint)centerOfViewport
{
    _customViewportCenterSet = YES;
    _centerOfViewport = centerOfViewport;
}

-(CGPoint)centerOfViewport
{
    if (!_customViewportCenterSet)
    {
        if (_layer != nil && _firstNode != nil && _secondNode != nil)
        {

            //NSLog(@"Nodes are: x%f,y%f and x%f,y%f",_firstNode.position.x,_firstNode.position.y,_secondNode.position.x,_secondNode.position.y);
            float x = _firstNode.position.x+((_secondNode.position.x-_firstNode.position.x)/2);
            float y = _firstNode.position.y+((_secondNode.position.y-_firstNode.position.y)/2);
            _centerOfViewport =  ccp(x,y);
            //NSLog(@"Center of viewport: x%f,y%f",_centerOfViewport.x,_centerOfViewport.y);
        }
    }
    return _centerOfViewport;
}

-(void)setCenterOfViewportOnLayer:(CGPoint)centerOfViewportOnLayer
{
    _centerOfViewportOnLayer = centerOfViewportOnLayer;
    
    _myInfo.position = _centerOfViewportOnLayer;
}
-(CGPoint)centerOfViewportOnLayer
{
    return _centerOfViewport;
}



-(void)setViewportRect:(CGRect)viewportRect
{
    _customViewportRectSet = YES;
    _viewportRect = viewportRect;
}

-(CGRect)viewportRect
{
    if (!_customViewportRectSet)
    {
        CGSize winSize = [CCDirector sharedDirector].winSize;
        _viewportRect = CGRectMake(_centerOfViewportOnLayer.x-(winSize.width*VIEWPORT_RECT_CLIPPING_COEFF)/2,
                                   _centerOfViewportOnLayer.y-(winSize.height*VIEWPORT_RECT_CLIPPING_COEFF)/2,
                                   (winSize.width*VIEWPORT_RECT_CLIPPING_COEFF),
                                   (winSize.height*VIEWPORT_RECT_CLIPPING_COEFF));
    }
    return _viewportRect;
}

-(void)toggleAutomaticViewportCenter
{
    _customViewportCenterSet = NO;
}

-(void)toggleAutomaticViewportRect
{
    _customViewportRectSet = NO;
}
@end
