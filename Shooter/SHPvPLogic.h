//
//  SHPvPLogic.h
//  Anti-heroes Wars
//
//  Created by Denis on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CWLSynthesizeSingleton.h"
#import "ProjectileActorInfo.h"

@class Player;
@class Base;
@class SHPlayerHealth;
@class SHBaseHealth;
@class HUDLayer;

@interface SHPvPLogic : NSObject
{
    HUDLayer *_myHUDLayer;
    Player *_firstPlayer;
    Player *_secondPlayer;
    SHPlayerHealth *_firstPlayerHealth;
    SHPlayerHealth *_secondPlayerHealth;
    
    Base *_firstBase;
    Base *_secondBase;
    SHBaseHealth *_firstBaseHealth;
    SHBaseHealth *_secondBaseHealth;
    
    CGPoint _firstPlayerSpawnPoint;
    CGPoint _secondPlayerSpawnPoint;
    
}
CWL_DECLARE_SINGLETON_FOR_CLASS(SHPvPLogic)

@property (nonatomic,assign) HUDLayer *myHUDLayer;

@property (nonatomic,assign) Player *firstPlayer;
@property (nonatomic,assign) Player *secondPlayer;
@property (nonatomic,assign) SHPlayerHealth *firstPlayerHealth;
@property (nonatomic,assign) SHPlayerHealth *secondPlayerHealth;
@property (nonatomic,assign) Base *firstBase;
@property (nonatomic,assign) Base *secondBase;
@property (nonatomic,assign) SHBaseHealth *firstBaseHealth;
@property (nonatomic,assign) SHBaseHealth *secondBaseHealth;
@property CGPoint firstPlayerSpawnPoint;
@property CGPoint secondPlayerSpawnPoint;

// player logic
-(Player*)opponentPlayerFor:(Player*)player;
-(SHPlayerHealth*)healthCounterForPlayer:(Player*)player;
-(void)player:(Player *)player gotHitByProjectile:(ProjectileActorInfo*)projInfo;
-(void)player:(Player*)player hitsOpponentPlayerWithProjectileInfo:(ProjectileActorInfo*)projInfo;
-(void)reportDeathFor:(Player*)player;
-(void)dealDamage:(int)dmg toPlayer:(Player*)player;
-(CGPoint)spawnPointForPlayer:(Player*)player;

// base logic
-(SHBaseHealth*)healthCounterForBase:(Base*)base;
-(void)base:(Base*)base gotHitByProjectile:(ProjectileActorInfo*)projInfo;
-(void)dealDamage:(int)dmg toBase:(Base*)base;
-(void)reportDestructionFor:(Base*)base;
@end
