//
//  PlayerController.h
//  SomeShooter
//
//  Created by Denis on 3/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "ZJoystick.h"
#import "Player.h"
#import "PlayerControllerJoystickDispatcher.h"

@interface PlayerController : CCNode <PlayerControllerJoystickDispatcherDelegate> {
    Player *_player;
    ZJoystick *joy1; //used to control movement
    ZJoystick *joy2; //used to control rotation
}

-(id)initWithPlayer:(Player*)player atCenterPoint:(CGPoint)centerPoint withMargin:(float)margin isVertical:(bool)vertical isReversed:(bool)reversed;

-(void)joystickControlBeganWithTag:(int)joyTag;
-(void)joystickControlDidUpdate:(id)joystick toXSpeedRatio:(CGFloat)xSpeedRatio toYSpeedRatio:(CGFloat)ySpeedRatio withTag:(int)joyTag;
-(void)joystickControlEndedWithTag:(int)joyTag;
-(void)joystickControlMovedWithTag:(int)joyTag;
@end
