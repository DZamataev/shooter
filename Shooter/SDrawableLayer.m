//
//  SDrawableLayer.m
//  Shooter
//
//  Created by Sergey on 3/27/12.
//  Copyright (c) 2012 DevitGroup. All rights reserved.
//

#import "SDrawableLayer.h"

@implementation SDrawableLayer
@synthesize clearColor, fadeSpeed, cleanChilden;

-(int)childrenCntUntilDraw {
    return childrenCntUntilDraw;
}
-(void)setChildrenCntUntilDraw:(int)newVal {
    
    if (childrenCntUntilDraw <= 1 &&
        timeCntUntilDraw == 0 &&
        newVal != 0)
            [self schedule:@selector(update:)];
    else if (childrenCntUntilDraw > 1 &&
             timeCntUntilDraw == 0 &&
             newVal <= 1)
        [self unschedule:@selector(update:)];
    
    childrenCntUntilDraw = newVal;
}

-(ccTime)timeCntUntilDraw {
    return timeCntUntilDraw;
}

-(void)setTimeCntUntilDraw:(ccTime)newVal {
    
    if (childrenCntUntilDraw <= 1 &&
        timeCntUntilDraw == 0 &&
        newVal != 0)
        [self schedule:@selector(update:)];
    else if (childrenCntUntilDraw > 1 &&
        timeCntUntilDraw == 0 &&
        newVal == 0)
        [self unschedule:@selector(update:)];
    
    timeCntUntilDraw = newVal;
}

-(id)init {
    self = [super init];
    
    childrenCntUntilDraw = 0;
    clearColor = ccc4f(0, 0, 0, 0);
    timeAccum = 0;
    timeCntUntilDraw = 0;
    fadeSpeed = 0;
    cleanChilden = YES;
    background = nil;
    
    return self;
}

- (void)drawAllChildrenWithCleanup:(BOOL)cleanup {
    
    if ([self.children count] == 0) return;
    
    CCSprite *newBackground = [self drawAllChildren];
    
    [self removeAllChildrenWithCleanup:cleanup];
    
    [self addChild:newBackground];
}

- (CCSprite*)drawAllChildren {
    
    if ([self.children count] == 0) return [CCSprite node];
    
    CGSize winSize = [CCDirector sharedDirector].winSize;
    CCRenderTexture *rt = [CCRenderTexture renderTextureWithWidth:winSize.width height:winSize.height];
    
    [rt beginWithClear:clearColor.r g:clearColor.g 
                     b:clearColor.b a:clearColor.a];
    
    [self visit];
    
    [rt end];
    
    CCSprite *newBackground = [CCSprite spriteWithTexture:rt.sprite.texture];
    
    newBackground.flipY = YES;
    newBackground.anchorPoint = CGPointZero;
    
    //ccTexParams tp = { GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE };
    //[newBackground.texture setTexParameters:&tp];
    
    return newBackground;
}

-(void)update:(ccTime)dt {
    
    timeAccum += dt;
    
    if((timeCntUntilDraw != 0.0 && 
        timeAccum > timeCntUntilDraw) ||
       (childrenCntUntilDraw > 1 && 
        [self.children count] > childrenCntUntilDraw))
    {
        CCSprite *newBackground = [self drawAllChildren];
        
        if (cleanChilden)
            [self removeAllChildrenWithCleanup:YES];
        else
            [background removeFromParentAndCleanup:YES];
        
        background = newBackground;
        background.opacity = 255 - fadeSpeed*dt;
        [self addChild:background];
        
        timeAccum -=timeCntUntilDraw;
    }
}

-(void)switchAutoCleanTo:(BOOL)isAutoClean {
    if (isAutoClean) {
        self.childrenCntUntilDraw = 10;
        self.timeCntUntilDraw = 0;
    }
    else {
        self.childrenCntUntilDraw = 0;
        self.timeCntUntilDraw = 0;
    }
}

+(SDrawableLayer*)layerWithPlume {
    
    SDrawableLayer *layer = [SDrawableLayer node];
    layer.timeCntUntilDraw = 1.0f/30;
    layer.fadeSpeed = 15;
    layer.cleanChilden = NO;
    return layer;
}
+(SDrawableLayer*)layerWithAutoclean {
    
    SDrawableLayer *layer = [SDrawableLayer node];
    layer.timeCntUntilDraw = 1.0f/30;
    
    return layer;
}
@end
