//
//  SHPlayerBehavior.m
//  Anti-heroes Wars
//
//  Created by Denis on 4/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SHPlayerBehavior.h"
#import "Player.h"
#import "PlayerLegs.h"
#import "PlayerBody.h"

@implementation SHPlayerBehavior
@synthesize player = _player;
@synthesize legs = _legs;
@synthesize body = _body;
-(id)initWithPlayer:(Player *)p andLegs:(PlayerLegs *)l andBody:(PlayerBody *)b
{
    self = [super init];
    _gonnaStop = NO;
    _player = p;
    _legs = l;
    _body = b;
    _bodyNotifyEnd = [[CCCallFunc actionWithTarget:self selector:@selector(notifyBodySeqEnd)] retain];
    _legsNotifyEnd = [[CCCallFunc actionWithTarget:self selector:@selector(notifyLegsSeqEnd)] retain];
    
    _bodyCheckEnd = [[CCCallFunc actionWithTarget:self selector:@selector(checkBodyEnd)] retain];
    _legsCheckEnd = [[CCCallFunc actionWithTarget:self selector:@selector(checkLegsEnd)] retain];
    
    _bodySeq = [[CCSequence actions:_body.leftBend, _bodyCheckEnd,_body.rightBend, _bodyNotifyEnd, nil] retain];
    _legsSeq = [[CCSequence actions:_legs.rightStep, _legsCheckEnd, _legs.leftStep, _legsNotifyEnd, nil] retain];
    return self;
}

-(void)standStill
{
    [_legs stopAllActionsOnSprite];
    [_body stopAllActionsOnSprite];
    [_legs runStepActionOnSprite:_legs.stillStep];
    [_body runBendActionOnSprite:_body.stillBend];
}

-(void)movingStatusChangedWithBool:(bool)isMoving
{
    if (isMoving == NO)
    {
//        NSLog(@"STOPP!!");
        _gonnaStop = YES;
    }
    else {
//        NSLog(@"START!!");
        _gonnaStop = NO;
        [self startRunLoop];
    }
}

-(void)startRunLoop
{
    [_legs stopAllActionsOnSprite];
    [_body stopAllActionsOnSprite];
    [_legs runStepActionOnSprite:_legsSeq];
    [_body runBendActionOnSprite:_bodySeq];
}

-(void)continueStepLoop
{
    [_legs stopAllActionsOnSprite];
    [_legs runStepActionOnSprite:_legsSeq];
}

-(void)continueBendLoop
{
    
    [_body stopAllActionsOnSprite];
    [_body runBendActionOnSprite:_bodySeq];
}


-(void)notifyBodySeqEnd
{
    if (_gonnaStop)
    {
        [self standStill];
    }
    else {
        [self continueBendLoop];
    }
}

-(void)notifyLegsSeqEnd
{
    if (_gonnaStop)
    {
        [self standStill];
    }
    else {
        [self continueStepLoop];
    }
}

-(void)checkLegsEnd
{
    if (_gonnaStop)
    {
        [self standStill];
    }
}

-(void)checkBodyEnd
{
    if (_gonnaStop)
    {
        [self standStill];
    }
}


-(void)dealloc
{
    _player = nil;
    _legs = nil;
    _body = nil;
    [_legsNotifyEnd release];
    [_bodyNotifyEnd release];
    [_bodySeq release];
    [_legsSeq release];
    [super dealloc];
}
@end
